import json
import requests
import codecs

data = json.loads(codecs.open("./data/spells.json", "r", "utf-8").read());

headers = {'Content-Type': 'application/json'}

for i in data:
	r = requests.post("http://127.0.0.1:9001/api/spell", data = i, headers = headers)
	print (r.text)
