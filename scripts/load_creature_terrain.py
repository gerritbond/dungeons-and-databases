import json
import requests
import codecs

data = json.loads(codecs.open("./data/monsters.json", "r", "utf-8").read());

headers = {'Content-Type': 'application/json'}
creatureTerrainMap={}
for creature in data:
    if creature['environment'] is not None:
        locations = [x.strip() for x in creature['environment'].split(',')]
        creatureTerrainMap[creature["name"]]=locations
    else:
        creatureTerrainMap[creature["name"]]=[]
r = json.loads(requests.get("http://127.0.0.1:9001/api/creatures", headers = headers).text)
statements=[]


terrains= json.loads(requests.get("http://127.0.0.1:9001/api/terrains", headers = headers).text)
terrainIdMap={}
for terrain in terrains:
    terrainIdMap[terrain['terrainName']]=terrain['id']


for i in r:
    id = i['id']
    locList=creatureTerrainMap[i['creatureName']]
    for loc in locList:
        print("insert into creature_terrain_xref(creature, terrain) values ("+str(id)+", "+str(terrainIdMap[loc])+");")

print(terrains)