#!/bin/bash
#
# Convenience script to spin up a local postgres database in a container to work on the application
#

docker run --name dnd_database -d -p 5432:5432 postgres 

