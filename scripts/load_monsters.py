import json
import requests
import codecs

data = json.loads(codecs.open("./data/monsters.json", "r", "utf-8").read());

headers = {'Content-Type': 'application/json'}

for i in data:
    print(i)
    req={
        'creatureName':i['name'],
        'alignment':"NEUTRAL_NEUTRAL",
        'size':i['size'],
        'type':i['type'],
        'armorClass':15,
        'hitPoints':i['hitPoints'],
        'hitDice':0,
        'speed':20,
        'challengeRating':i['cr'],
        'experienceReward':0
    }
    r = requests.post("http://127.0.0.1:9001/api/creatures", data = json.JSONEncoder().encode(req), headers = headers)
    print (r.text)
