import React, { Component } from 'react';
import MonsterBlock from './Monsters';
import SpellCardList from './SpellCard';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import PlayerForm from "./PlayerForm";
import PlayerView from "./PlayerView";


class RouteManager extends React.Component{
    render(){
        return (
            <Router>
                <div className="app">
                    <div className="nav-bar">
                        <span className="left-header-text">
                            <Link to="/">DUNGEONS & DATABASES</Link>
                        </span>
                        <span className="right-header-text">
                            <span>
                                <Link to="/players">Parties</Link>
                            </span>
                            <span>
                                <Link to="/monsters">Encounters</Link>
                            </span>
                            <span>
                                <Link to="/spells">Spells</Link>
                            </span>
                        </span>


                    </div>

                    <Route exact path="/" component={Home} />
                    <Route exact path="/players" component={Players} />
                    <Route exact path="/monsters" component={Monsters} />
                    <Route exact path="/spells" component={Spells} />


                </div>
            </Router>

        );
    }
}

const Home = () => (
    <div className="content-container">
        <div className="content title">
            <h2 className="">Dungeons & Databases</h2>
            <h4 className="">Gerrit Bond and Andrew Ross</h4>
        </div>
    </div>

);

const Players = () => (
    <div className="content-container">
        <div className="content">
            <PlayerView/>
        </div>
    </div>

);

const Monsters = () => (
    <div className="content-container">
        <div className="content">
            <MonsterBlock />
        </div>
    </div>

);


const Spells = () => (
    <div className="content-container">
        <div className="content">
            <SpellCardList />
        </div>
    </div>

);


export default RouteManager;
