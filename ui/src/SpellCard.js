import React from 'react';

export default class SpellCard extends React.Component{
    constructor(props){
        super(props);
        this.state={
            spells:[]
        }
    }

    componentDidMount(){
        fetch('/api/spell')
            .then(results=>{return results.json()})
            .then(data=>{
                let spells = data.map(spell=>{
                    return (
                        <div key={spell.spellId} className="spell-card">
                            <h3 className="spell-title">{spell.spellName}</h3>
                            <div className="spell-details">Components: {spell.components}</div>
                            <div>Casting Time: {spell.castingTime}</div>
                            <div>Duration: {spell.duration}</div>
                            <div>Range: {spell.range}</div>
                            <div>{(spell.level===0?spell.school+' Cantrip':'Level '+spell.level+' '+spell.school+' Spell')}</div>
                            <div>{spell.description}</div>
                        </div>
                    )
                });
                this.setState({spells:spells});
            })
    }

    render(){
        return (
            <div className="content-view">{this.state.spells}</div>
        );
    }
}



