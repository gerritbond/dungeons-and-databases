import React from 'react';
import PlayerForm from "./PlayerForm";
import Button from '@material-ui/core/Button';

export default class PlayerView extends React.Component {
    constructor(props) {
        super(props);
        this.addPlayerCard = this.addPlayerCard.bind(this);
        this.state = {
            players: [],
            parties:[],
            selectedParty:''
        }
    }

    addPlayerCard(){
        let players = this.state.players;
        players.push({});
        this.setState({players:players});
    }

    selectParty(partyId){
        this.setState({selectedParty:partyId})
    }

    saveParty(){
        //grab the selected party and save it. update the state to contain the new party listing
    }


    render(){
        return (
            <div className="player-view content-view">
                <div className="left-list-pane">
                    {
                        Object
                            .keys(this.state.parties)
                            .map(key => {if(key) return(<div key={this.state.parties[key].id}
                                                             onClick={()=>this.selectParty(this.state.parties[key].id)}>{this.state.parties[key]}</div>)})
                    }
                </div>
                <div className="right-content-pane">
                    {
                        Object
                            .keys(this.state.players)
                            .map(key => {if(key) return (<PlayerForm key={this.state.players[key].id+'-form'} player={this.state.players[key]}/>)})
                    }
                    <Button
                        onClick={() => this.addPlayerCard(this.state.selectedParty)}>Add New Player</Button>
                    <Button onClick={() => this.saveParty(this.state.selectedParty)}>Save Party</Button>
                </div>
            </div>
        )
    }
}

