import React from 'react';


export default class PlayerForm extends React.Component{
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.playerName = React.createRef();
        this.characterName = React.createRef();
        this.level = React.createRef();
        this.backstory = React.createRef();
        this.experience = React.createRef();
        this.hitpoints = React.createRef();
    }

    handleSubmit(event) {
        event.preventDefault();
        alert('A name was submitted: ' + this.playerName.current.value +'\nCharacter: '+this.characterName.current.value);
    }

    render(){
        return (
            <div>
                <form onSubmit={this.handleSubmit} id="player-form">
                    <label>
                        Name:
                        <input type="text" ref={this.playerName}/>
                    </label>
                    <label>
                        Character Name:
                        <input type="text" ref={this.characterName}/>
                    </label>
                    <label>
                        Level
                        <input type="text" ref={this.level}/>
                    </label>
                    <label>
                        XP:
                        <input type="text" ref={this.experience}/>
                    </label>
                    <label>
                        Hitpoint Total:
                        <input type="text" ref={this.hitpoints}/>
                    </label>
                    <input type="submit" value="Submit" />
                </form>
            </div>

        );
    }
}



