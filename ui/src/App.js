import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import RouteManager from './RouteManager';



export default class App extends Component {
  render() {
    return (
      <div>
          <CssBaseline />
          <RouteManager></RouteManager>
      </div>
    );
  }
}









