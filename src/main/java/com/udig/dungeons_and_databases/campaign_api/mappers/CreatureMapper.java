package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.enums.Alignment;
import com.udig.dungeons_and_databases.campaign_api.enums.Size;
import com.udig.dungeons_and_databases.campaign_api.enums.Type;
import com.udig.dungeons_and_databases.campaign_api.models.Creature;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.CREATURE;


@Component
public class CreatureMapper extends ClassIdentifyingRecordMapper<Record, Creature> {
    public CreatureMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Creature> getTargetClass() {
        return Creature.class;
    }

    @Override
    public Creature map(Record record) {
        return new Creature(
            record.get(CREATURE.CREATURE_ID),
            record.get(CREATURE.CREATURE_NAME),
            Alignment.values()[record.get(CREATURE.ALIGNMENT)],
            Size.values()[record.get(CREATURE.SIZE)],
            Type.values()[record.get(CREATURE.TYPE)],
            record.get(CREATURE.ARMOR_CLASS),
            record.get(CREATURE.HITPOINTS),
            record.get(CREATURE.HITDICE),
            record.get(CREATURE.SPEED),
            record.get(CREATURE.CHALLENGE_RATING),
            record.get(CREATURE.EXPERIENCE_REWARD)
        );
    }
}
