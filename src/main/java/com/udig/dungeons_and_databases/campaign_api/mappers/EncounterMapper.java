package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Encounter;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.ENCOUNTER;


@Component
public class EncounterMapper extends ClassIdentifyingRecordMapper<Record, Encounter> {
    public EncounterMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Encounter> getTargetClass() {
        return Encounter.class;
    }

    @Override
    public Encounter map(Record record) {
        return new Encounter(
            record.get(ENCOUNTER.ENCOUNTER_ID),
            record.get(ENCOUNTER.CHALLENGE_RATING),
            record.get(ENCOUNTER.EXPERIENCE_REWARD),
            record.get(ENCOUNTER.TERRAIN)
        );
    }
}
