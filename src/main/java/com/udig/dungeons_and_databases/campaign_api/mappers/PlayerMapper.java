package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Player;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER;

@Component
public class PlayerMapper extends ClassIdentifyingRecordMapper<Record, Player> {
    @Autowired
    public PlayerMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Player> getTargetClass() {
        return Player.class;
    }

    @Override
    public Player map(Record record) {
        return new Player(
                record.get(PLAYER.PLAYER_ID),
                record.get(PLAYER.FIRST_NAME),
                record.get(PLAYER.LAST_NAME));
    }
}
