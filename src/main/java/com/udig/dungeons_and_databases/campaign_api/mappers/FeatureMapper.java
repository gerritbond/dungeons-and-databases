package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Ability;
import com.udig.dungeons_and_databases.campaign_api.models.Feature;
import com.udig.dungeons_and_databases.campaign_api.models.Skill;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.*;


@Component
public class FeatureMapper extends ClassIdentifyingRecordMapper<Record, Feature> {
    public FeatureMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Feature> getTargetClass() {
        return Feature.class;
    }

    @Override
    public Feature map(Record record) {
        return new Feature(
            record.get(FEATURE.FEATURE_ID),
            record.get(FEATURE.FEATURE_NAME),
            record.get(FEATURE.DESCRIPTION),
            new Skill(
                    record.get(SKILL.SKILL_ID),
                    record.get(SKILL.SKILL_NAME),
                    new Ability(
                            record.get(ABILITY.ABILITY_ID),
                            record.get(ABILITY.ABILITY_NAME),
                            record.get(ABILITY.ABBREVIATION)
                    )
            )
        );
    }
}
