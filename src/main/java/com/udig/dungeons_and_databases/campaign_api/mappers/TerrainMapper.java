package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Terrain;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.TERRAIN;


@Component
public class TerrainMapper extends ClassIdentifyingRecordMapper<Record, Terrain> {
    public TerrainMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Terrain> getTargetClass() {
        return Terrain.class;
    }

    @Override
    public Terrain map(Record record) {
        return new Terrain(
                record.get(TERRAIN.TERRAIN_ID),
                record.get(TERRAIN.TERRAIN_NAME),
                record.get(TERRAIN.DESCRIPTION)
        );
    }
}
