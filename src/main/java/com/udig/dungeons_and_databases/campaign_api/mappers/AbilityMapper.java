package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Ability;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.ABILITY;
import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER_CHARACTER_ABILITY_XREF;


@Component
public class AbilityMapper extends ClassIdentifyingRecordMapper<Record, Ability> {
    public AbilityMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Ability> getTargetClass() {
        return Ability.class;
    }

    @Override
    public Ability map(Record record) {
        return new Ability(
            record.get(ABILITY.ABILITY_ID),
            record.get(ABILITY.ABILITY_NAME),
            record.get(ABILITY.ABBREVIATION),
            record.get(PLAYER_CHARACTER_ABILITY_XREF.RANK),
            record.get(PLAYER_CHARACTER_ABILITY_XREF.PROFICIENT)
        );
    }
}
