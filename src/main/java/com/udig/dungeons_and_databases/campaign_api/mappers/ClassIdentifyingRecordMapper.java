package com.udig.dungeons_and_databases.campaign_api.mappers;

import org.jooq.Record;
import org.jooq.RecordMapper;

public abstract class ClassIdentifyingRecordMapper<R extends Record, E> implements RecordMapper<R, E> {
    public ClassIdentifyingRecordMapper (IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        identifyingRecordMapperProvider.addMapping(this);
    }

    public abstract Class <E> getTargetClass ();
}
