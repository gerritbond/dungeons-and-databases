package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Item;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.ITEM;
import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER_CHARACTER_ITEM_XREF;


@Component
public class ItemMapper extends ClassIdentifyingRecordMapper<Record, Item> {
    public ItemMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Item> getTargetClass() {
        return Item.class;
    }

    @Override
    public Item map(Record record) {
        Integer currentCost = record.field(PLAYER_CHARACTER_ITEM_XREF.CURRENT_COST_CP) != null ?
                record.get(PLAYER_CHARACTER_ITEM_XREF.CURRENT_COST_CP) : record.get(ITEM.BASE_COST_CP);

        Integer currentQuantity = record.field(PLAYER_CHARACTER_ITEM_XREF.CURRENT_QUANTITY) != null ?
                record.get(PLAYER_CHARACTER_ITEM_XREF.CURRENT_QUANTITY) : record.get(ITEM.BASE_QUANTITY);


        return new Item(
            record.get(ITEM.ITEM_ID),
            record.get(ITEM.ITEM_NAME),
            record.get(ITEM.DESCRIPTION),
            record.get(ITEM.BASE_COST_CP),
            record.get(ITEM.BASE_QUANTITY),
            currentCost,
            currentQuantity
        );
    }
}
