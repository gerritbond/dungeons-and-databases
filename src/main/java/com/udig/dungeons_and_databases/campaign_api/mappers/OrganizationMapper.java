package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Organization;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.ORGANIZATION;


@Component
public class OrganizationMapper extends ClassIdentifyingRecordMapper<Record, Organization> {
    public OrganizationMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Organization> getTargetClass() {
        return Organization.class;
    }

    @Override
    public Organization map(Record record) {
        return new Organization (
            record.get(ORGANIZATION.ORGANIZATION_ID),
            record.get(ORGANIZATION.ORGANIZATION_NAME),
            record.get(ORGANIZATION.DESCRIPTION)
        );
    }
}
