package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Race;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.RACE;


@Component
public class RaceMapper extends ClassIdentifyingRecordMapper<Record, Race> {
    public RaceMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Race> getTargetClass() {
        return Race.class;
    }

    @Override
    public Race map(Record record) {
        return new Race (
            record.get(RACE.RACE_ID),
            record.get(RACE.RACE_NAME),
            record.get(RACE.DESCRIPTION)
        );
    }
}
