package com.udig.dungeons_and_databases.campaign_api.mappers;

import org.jooq.RecordMapperProvider;

public interface IdentifyingRecordMapperProvider extends RecordMapperProvider {
    void addMapping (ClassIdentifyingRecordMapper<?, ?> mapper);
}
