package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Spell;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.SPELL;

@Component
public class SpellMapper extends ClassIdentifyingRecordMapper<Record, Spell> {
    public SpellMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Spell> getTargetClass() {
        return Spell.class;
    }

    @Override
    public Spell map(Record record) {
        return new Spell(
                record.get(SPELL.SPELL_ID),
                record.get(SPELL.SPELL_NAME),
                record.get(SPELL.CASTING_TIME),
                record.get(SPELL.COMPONENTS),
                record.get(SPELL.DESCRIPTION),
                record.get(SPELL.DURATION),
                record.get(SPELL.LEVEL),
                record.get(SPELL.RANGE),
                record.get(SPELL.SCHOOL)
        );
    }
}
