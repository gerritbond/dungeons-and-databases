package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Clazz;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.CLAZZ;


@Component
public class ClazzMapper extends ClassIdentifyingRecordMapper<Record, Clazz> {
    public ClazzMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Clazz> getTargetClass() {
        return Clazz.class;
    }

    @Override
    public Clazz map(Record record) {
        return new Clazz(
            record.get(CLAZZ.CLAZZ_ID),
            record.get(CLAZZ.CLAZZ_NAME),
            record.get(CLAZZ.DESCRIPTION)
        );
    }
}
