package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Ability;
import com.udig.dungeons_and_databases.campaign_api.models.Skill;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.*;


@Component
public class SkillMapper extends ClassIdentifyingRecordMapper<Record, Skill> {
    public SkillMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<Skill> getTargetClass() {
        return Skill.class;
    }

    @Override
    public Skill map(Record record) {
        return new Skill (
            record.get(SKILL.SKILL_ID),
            record.get(SKILL.SKILL_NAME),
            new Ability(
                    record.get(ABILITY.ABILITY_ID),
                    record.get(ABILITY.ABILITY_NAME),
                    record.get(ABILITY.ABBREVIATION),
                    record.get(PLAYER_CHARACTER_ABILITY_XREF.RANK),
                    record.get(PLAYER_CHARACTER_ABILITY_XREF.PROFICIENT)
            )
        );
    }
}
