package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.enums.Alignment;
import com.udig.dungeons_and_databases.campaign_api.models.PlayerCharacter;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER_CHARACTER;


@Component
public class PlayerCharacterMapper extends ClassIdentifyingRecordMapper<Record, PlayerCharacter> {
    public PlayerCharacterMapper(IdentifyingRecordMapperProvider identifyingRecordMapperProvider) {
        super(identifyingRecordMapperProvider);
    }

    @Override
    public Class<PlayerCharacter> getTargetClass() {
        return PlayerCharacter.class;
    }

    @Override
    public PlayerCharacter map(Record record) {
        return new PlayerCharacter(
            record.get(PLAYER_CHARACTER.CHARACTER_ID),
            record.get(PLAYER_CHARACTER.CHARACTER_NAME),
            record.get(PLAYER_CHARACTER.BACKGROUND),
            record.get(PLAYER_CHARACTER.BACKSTORY),
            Alignment.values()[record.get(PLAYER_CHARACTER.ALIGNMENT)],
            record.get(PLAYER_CHARACTER.EXPERIENCE),
            record.get(PLAYER_CHARACTER.ARMOR_CLASS),
            record.get(PLAYER_CHARACTER.INSPIRATION),
            record.get(PLAYER_CHARACTER.PROFICIENCY_BONUS),
            record.get(PLAYER_CHARACTER.AGE),
            record.get(PLAYER_CHARACTER.HEIGHT),
            record.get(PLAYER_CHARACTER.WEIGHT),
            record.get(PLAYER_CHARACTER.EYES),
            record.get(PLAYER_CHARACTER.SKIN),
            record.get(PLAYER_CHARACTER.HAIR),
            record.get(PLAYER_CHARACTER.HITPOINTS),
            record.get(PLAYER_CHARACTER.CURRENT_HITPOINTS),
            record.get(PLAYER_CHARACTER.TEMPORARY_HITPOINTS),
            record.get(PLAYER_CHARACTER.PERSONALITY_TRAITS),
            record.get(PLAYER_CHARACTER.IDEALS),
            record.get(PLAYER_CHARACTER.BONDS),
            record.get(PLAYER_CHARACTER.FLAWS),
            record.get(PLAYER_CHARACTER.RACE),
            record.get(PLAYER_CHARACTER.PLAYER),
            record.get(PLAYER_CHARACTER.PARTY)
        );
    }
}
