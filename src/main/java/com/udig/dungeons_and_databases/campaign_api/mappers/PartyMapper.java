package com.udig.dungeons_and_databases.campaign_api.mappers;

import com.udig.dungeons_and_databases.campaign_api.models.Party;
import com.udig.dungeons_and_databases.campaign_api.models.Player;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PARTY;
import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER;

@Component
public class PartyMapper extends ClassIdentifyingRecordMapper<Record,Party> {
    @Autowired
    public PartyMapper (final IdentifyingRecordMapperProvider recordMapperProvider) {
        super(recordMapperProvider);
    }

    @Override
    public Party map(Record record) {
        return new Party(
                record.get(PARTY.PARTY_ID),
                record.get(PARTY.NAME),
                    new Player(
                            record.get(PLAYER.PLAYER_ID),
                            record.get(PLAYER.FIRST_NAME),
                            record.get(PLAYER.LAST_NAME)));
    }

    @Override
    public Class<Party> getTargetClass() {
        return Party.class;
    }
}
