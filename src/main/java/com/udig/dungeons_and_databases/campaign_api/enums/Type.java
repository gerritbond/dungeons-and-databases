package com.udig.dungeons_and_databases.campaign_api.enums;

public enum Type {
    ABERRATIONS,
    BEASTS,
    CELESTIALS,
    CONSTRUCTS,
    DRAGONS,
    ELEMENTAL,
    FEY,
    FIENDS,
    GIANTS,
    HUMANOIDS,
    MONSTROSITIES,
    OOZES,
    PLANTS,
    UNDEAD
}
