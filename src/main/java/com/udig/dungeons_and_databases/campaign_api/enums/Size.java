package com.udig.dungeons_and_databases.campaign_api.enums;

public enum Size {
    FINE,
    DIMINUTIVE,
    TINY,
    SMALL,
    MEDIUM,
    LARGE,
    HUGE,
    GARGANTUAN,
    COLOSSAL;
}
