package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Item;
import com.udig.dungeons_and_databases.campaign_api.services.NestedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping({"/characters/{base_id}/items"})
@Controller
public class PlayerCharacterItemController extends NestedController<Item> {
    @Autowired
    public PlayerCharacterItemController(final NestedService<Item> playerCharacterItemService) {
        super(playerCharacterItemService);
    }
}