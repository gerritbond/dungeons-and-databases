package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

public class BaseController<S> {
    private final BaseService<S> service;

    public BaseController (BaseService<S> service) {
        this.service = service;
    }

    @RequestMapping(value = {"", "/"}, method = GET)
    public ResponseEntity<List<S>> find () {
        return service.find().toResponseEntity();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity<S> find (@PathVariable(value = "id") final Integer id) {
        return service.find(id).toResponseEntity();
    }

    @RequestMapping(value = {"", "/"}, method = POST)
    public ResponseEntity<S> create (@RequestBody final S s) {
        return service.create(s).toResponseEntity();
    }

    @RequestMapping(value = {"", "/"}, method = PUT)
    public ResponseEntity<S> update(@RequestBody final S s) {
        return service.update(s).toResponseEntity();
    }

    @RequestMapping(value = {"", "/{id}"}, method = DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable(value="id") final Integer id) {return service.delete(id).toResponseEntity();}
}
