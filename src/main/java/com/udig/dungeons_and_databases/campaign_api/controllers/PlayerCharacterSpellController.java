package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Spell;
import com.udig.dungeons_and_databases.campaign_api.services.NestedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/characters/{base_id}/spells")
@Controller
public class PlayerCharacterSpellController extends NestedController<Spell> {
    @Autowired
    public PlayerCharacterSpellController(final NestedService<Spell> spellService) {
        super(spellService);
    }
}
