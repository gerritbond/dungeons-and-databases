package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Creature;
import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/creatures")
@Controller
public class CreatureController extends BaseController<Creature> {
    @Autowired
    public CreatureController(BaseService<Creature> service) {
        super(service);
    }
}
