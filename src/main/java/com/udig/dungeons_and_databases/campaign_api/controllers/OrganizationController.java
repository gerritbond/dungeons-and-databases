package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Organization;
import com.udig.dungeons_and_databases.campaign_api.services.NestedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/characters/{base_id}/organizations")
@Controller
public class OrganizationController extends NestedController<Organization> {
    @Autowired
    public OrganizationController(final NestedService<Organization> organizationService) {
        super(organizationService);
    }
}