package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Terrain;
import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/terrains")
@Controller
public class TerrainController extends BaseController<Terrain> {

    @Autowired
    public TerrainController(BaseService<Terrain> service) {
        super(service);
    }
}
