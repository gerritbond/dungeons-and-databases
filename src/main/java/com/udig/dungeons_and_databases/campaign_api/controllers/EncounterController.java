package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Creature;
import com.udig.dungeons_and_databases.campaign_api.models.Encounter;
import com.udig.dungeons_and_databases.campaign_api.services.CreatureService;
import com.udig.dungeons_and_databases.campaign_api.services.EncounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/encounters")
@Controller
public class EncounterController extends BaseController<Encounter> {
    private EncounterService service;
    private CreatureService creatureService;

    @Autowired
    public EncounterController(EncounterService service, CreatureService creatureService) {
        super(service);
        this.service = service;
        this.creatureService = creatureService;
    }

    @RequestMapping("/parties/{party_id}")
    public ResponseEntity<Encounter> generateForParty (@RequestBody Encounter encounter,
                                                       @PathVariable("party_id") Integer partyId) {
        return service.generate(encounter, partyId).toResponseEntity();
    }

    @RequestMapping("/{id}/creatures")
    public ResponseEntity<List<Creature>> createsForEncounter (@PathVariable("id") Integer encounterId) {
        List<Integer> creatureIds = service.creaturesForEncounter(encounterId).getResponse();

        return creatureService.find(creatureIds.toArray(new Integer [creatureIds.size()])).toResponseEntity();
    }
}
