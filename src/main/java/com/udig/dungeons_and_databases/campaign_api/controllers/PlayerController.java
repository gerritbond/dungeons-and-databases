package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Party;
import com.udig.dungeons_and_databases.campaign_api.models.Player;
import com.udig.dungeons_and_databases.campaign_api.models.PlayerCharacter;
import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import com.udig.dungeons_and_databases.campaign_api.services.PartyService;
import com.udig.dungeons_and_databases.campaign_api.services.PlayerCharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("/players")
@Controller
public class PlayerController extends BaseController<Player>{
    private final PlayerCharacterService playerCharacterService;
    private final PartyService partyService;

    @Autowired
    public PlayerController (final BaseService<Player> playerService,
                             final PlayerCharacterService playerCharacterService,
                             final PartyService partyService) {
        super(playerService);
        this.playerCharacterService = playerCharacterService;
        this.partyService = partyService;
    }

    @RequestMapping(value = "/{id}/characters", method = GET)
    public ResponseEntity<List<PlayerCharacter>> findPlayerCharacters (@PathVariable(value = "id") final Integer id) {
        return playerCharacterService.findForPlayer(id).toResponseEntity();
    }

    @RequestMapping(value = "/{id}/parties", method = GET)
    public ResponseEntity<List<Party>> findPlayerParties (@PathVariable(value = "id") final Integer id) {
        List<Integer> partyIds =
                playerCharacterService.findForPlayer(id).getResponse().stream().map(PlayerCharacter::getParty).collect(Collectors.toList());

        return partyService.findAll(partyIds).toResponseEntity();
    }
}
