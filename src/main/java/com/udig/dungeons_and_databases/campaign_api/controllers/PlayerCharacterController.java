package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.PlayerCharacter;
import com.udig.dungeons_and_databases.campaign_api.services.PlayerCharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/characters")
@Controller
public class PlayerCharacterController extends BaseController<PlayerCharacter> {
    @Autowired
    public PlayerCharacterController(final PlayerCharacterService playerCharacterService) {
        super(playerCharacterService);
    }
}