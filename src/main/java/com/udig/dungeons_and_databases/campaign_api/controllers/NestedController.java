package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.services.NestedService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

public class NestedController<S> {
    private NestedService<S> service;

    public NestedController(NestedService<S> service) {
        this.service = service;
    }

    @RequestMapping(value = {"", "/"}, method = GET)
    public ResponseEntity<List<S>> find (@PathVariable(value = "base_id") final Integer baseId) {
        return service.find(baseId).toResponseEntity();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity<S> find (@PathVariable(value = "base_id") final Integer baseId, @PathVariable(value = "id") final Integer id) {
        return service.find(baseId, id).toResponseEntity();
    }

    @RequestMapping(value = {"", "/"}, method = POST)
    public ResponseEntity<S> create (@PathVariable(value = "base_id") final Integer baseId, @RequestBody final S s) {
        return service.create(baseId, s).toResponseEntity();
    }

    @RequestMapping(value = {"", "/"}, method = PUT)
    public ResponseEntity<S> update(@PathVariable(value = "base_id") final Integer baseId, @RequestBody final S s) {
        return service.update(baseId, s).toResponseEntity();
    }

}
