package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Ability;
import com.udig.dungeons_and_databases.campaign_api.services.PlayerCharacterAbilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/characters/{base_id}/abilities")
@Controller
public class AbilityController extends NestedController<Ability> {
    @Autowired
    public AbilityController(final PlayerCharacterAbilityService abilityService) {
        super(abilityService);
    }
}