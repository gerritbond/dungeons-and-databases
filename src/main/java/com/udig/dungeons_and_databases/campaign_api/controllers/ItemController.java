package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Item;
import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping({"/items"})
@Controller
public class ItemController extends BaseController<Item> {
    @Autowired
    public ItemController(final BaseService<Item> itemService) {
        super(itemService);
    }
}