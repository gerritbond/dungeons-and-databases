package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Skill;
import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/skills")
@Controller
public class SkillController extends BaseController<Skill> {
    @Autowired
    public SkillController(final BaseService<Skill> skillService) {
        super(skillService);
    }
}