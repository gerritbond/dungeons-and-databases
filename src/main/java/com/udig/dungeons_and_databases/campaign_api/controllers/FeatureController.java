package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Feature;
import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/features")
@Controller
public class FeatureController extends BaseController<Feature> {
    @Autowired
    public FeatureController(final BaseService<Feature> featureService) {
        super(featureService);
    }
}