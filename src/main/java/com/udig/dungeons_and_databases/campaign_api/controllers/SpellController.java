package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Spell;
import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/spells")
@Controller
public class SpellController extends BaseController<Spell> {
    @Autowired
    public SpellController (final BaseService<Spell> spellService) {
        super(spellService);
    }
}
