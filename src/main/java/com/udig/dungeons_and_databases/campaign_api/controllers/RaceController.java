package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Race;
import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/races")
@Controller
public class RaceController extends BaseController<Race> {
    @Autowired
    public RaceController(final BaseService<Race> raceService) {
        super(raceService);
    }
}