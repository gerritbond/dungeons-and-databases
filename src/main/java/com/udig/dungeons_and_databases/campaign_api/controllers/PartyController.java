package com.udig.dungeons_and_databases.campaign_api.controllers;

import com.udig.dungeons_and_databases.campaign_api.models.Party;
import com.udig.dungeons_and_databases.campaign_api.models.Player;
import com.udig.dungeons_and_databases.campaign_api.models.PlayerCharacter;
import com.udig.dungeons_and_databases.campaign_api.services.BaseService;
import com.udig.dungeons_and_databases.campaign_api.services.PlayerCharacterService;
import com.udig.dungeons_and_databases.campaign_api.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("/parties")
@Controller
public class PartyController extends BaseController<Party> {
    private PlayerCharacterService playerCharacterService;
    private PlayerService playerService;

    @Autowired
    public PartyController (final BaseService<Party> partyService,
                            PlayerCharacterService playerCharacterService,
                            PlayerService playerService) {
        super(partyService);
        this.playerCharacterService = playerCharacterService;
        this.playerService = playerService;
    }

    @RequestMapping(value = "/{id}/characters", method = GET)
    public ResponseEntity<List<PlayerCharacter>> findPartyCharacters (@PathVariable(value = "id") final Integer id) {
        return playerCharacterService.findForParty(id).toResponseEntity();
    }

    @RequestMapping(value = "/{id}/players", method = GET)
    public ResponseEntity<List<Player>> findPartyPlayers (@PathVariable(value = "id") final Integer id) {
        List<Integer> playerIds =
                playerCharacterService.findForParty(id).getResponse().stream().map(PlayerCharacter::getPlayer).collect(Collectors.toList());

        return playerService.findAll(playerIds).toResponseEntity();
    }
}