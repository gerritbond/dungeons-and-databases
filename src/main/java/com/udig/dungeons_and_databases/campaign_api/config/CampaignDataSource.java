package com.udig.dungeons_and_databases.campaign_api.config;

import com.udig.dungeons_and_databases.campaign_api.mappers.ClassIdentifyingRecordMapper;
import com.udig.dungeons_and_databases.campaign_api.mappers.IdentifyingRecordMapperProvider;
import com.zaxxer.hikari.HikariDataSource;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.RecordType;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultRecordMapper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource("classpath:application.properties")
public class CampaignDataSource {
    private final Environment env;

    public CampaignDataSource (final Environment env) {
        this.env = env;
    }

    @PostConstruct
    public void postConstruct () {
        configuration().setRecordMapperProvider(recordMapperProvider());
    }

    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    @Bean
    @Primary
    DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();

        dataSource.setDriverClassName(env.getRequiredProperty("spring.datasource.driver-class-name"));
        dataSource.setJdbcUrl(env.getRequiredProperty("spring.datasource.url"));
        dataSource.setUsername(env.getRequiredProperty("spring.datasource.username"));
        dataSource.setPassword(env.getRequiredProperty("spring.datasource.password"));

        dataSource.setMaximumPoolSize(Integer.parseInt(env.getRequiredProperty("pool.size.max")));
        dataSource.setMinimumIdle(Integer.parseInt(env.getRequiredProperty("pool.size.min")));

        dataSource.setAutoCommit(true);
        return dataSource;
    }

    @Bean
    public LazyConnectionDataSourceProxy lazyConnectionDataSource() {
        return new LazyConnectionDataSourceProxy(dataSource());
    }

    @Bean
    public TransactionAwareDataSourceProxy transactionAwareDataSource() {
        return new TransactionAwareDataSourceProxy(lazyConnectionDataSource());
    }

    @Bean
    @Primary
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(lazyConnectionDataSource());
    }

    @Bean
    public DataSourceConnectionProvider connectionProvider() {
        return new DataSourceConnectionProvider(transactionAwareDataSource());
    }

    @Bean
    public IdentifyingRecordMapperProvider recordMapperProvider () {
        return new IdentifyingRecordMapperProvider() {
            private final Map<Class, RecordMapper> mappers = new HashMap<>();

            @Override
            public <R extends Record, E> RecordMapper<R, E> provide(RecordType<R> recordType, Class<? extends E> type) {
                if(mappers.containsKey(type))
                    return mappers.get(type);

                // TODO Find out why the Default Record mapper is not working
                return new DefaultRecordMapper(recordType, type);
            }

            @Override
            public void addMapping (ClassIdentifyingRecordMapper<?, ?> mapper) {
                mappers.put(mapper.getTargetClass(), mapper);
            }
        };
    }

    @Bean
    public DefaultConfiguration configuration() {
        DefaultConfiguration jooqConfiguration = new DefaultConfiguration();

        jooqConfiguration.set(connectionProvider());

        String sqlDialectName = env.getRequiredProperty("spring.jooq.sql-dialect");

        SQLDialect dialect = SQLDialect.valueOf(sqlDialectName);
        jooqConfiguration.set(dialect);

        return jooqConfiguration;
    }

    @Bean
    public DefaultDSLContext dslContext() {
        return new DefaultDSLContext(configuration());
    }

}
