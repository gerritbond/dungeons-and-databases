package com.udig.dungeons_and_databases.campaign_api.models;

import java.util.ArrayList;
import java.util.List;

public class Party {
    private Integer id;
    private List<PlayerCharacter> characters;
    private Player gameMaster;
    private String name;

    public Party() {

    }

    public Party(final Integer id) {
        this.id = id;
    }

    public Party(final Integer id, String name, final Player gameMaster) {
        this.id = id;
        this.name = name;
        this.gameMaster = gameMaster;
        this.characters = new ArrayList<>();
    }

    public Party(String name, final Player gameMaster) {
        this.name = name;
        this.gameMaster = gameMaster;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public List<PlayerCharacter> getCharacters() {
        return characters;
    }

    public void setCharacters (List<PlayerCharacter> characters ) {
        this.characters = characters;
    }

    public Player getGameMaster() {
        return gameMaster;
    }

    public void setGameMaster(Player player) {
        this.gameMaster = player;
    }

    public String getName() {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }
}
