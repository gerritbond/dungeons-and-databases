package com.udig.dungeons_and_databases.campaign_api.models;

public class Item {
    private Integer id;
    private String name;
    private String description;
    private Integer baseCost;
    private Integer baseQuantity;
    private Integer currentCost;
    private Integer currentQuantity;

    public Item () {

    }

    public Item (Integer id, String name, String description,
                 Integer baseCost, Integer baseQuantity,
                 Integer currentCost, Integer currentQuantity) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.baseCost = baseCost;
        this.baseQuantity = baseQuantity;
        this.currentCost = currentCost;
        this.currentQuantity = currentQuantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getBaseCost() {
        return baseCost;
    }

    public void setBaseCost(Integer baseCost) {
        this.baseCost = baseCost;
    }

    public Integer getBaseQuantity() {
        return baseQuantity;
    }

    public void setBaseQuantity(Integer baseQuantity) {
        this.baseQuantity = baseQuantity;
    }

    public Integer getCurrentCost() {
        return currentCost;
    }

    public void setCurrentCost(Integer currentCost) {
        this.currentCost = currentCost;
    }

    public Integer getCurrentQuantity() {
        return currentQuantity;
    }

    public void setCurrentQuantity(Integer currentQuantity) {
        this.currentQuantity = currentQuantity;
    }
}
