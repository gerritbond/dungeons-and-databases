package com.udig.dungeons_and_databases.campaign_api.models;

public class Ability {
    private Integer id;
    private String name;
    private String abbreviation;
    private Integer rank;
    private Boolean proficient;

    public Ability () {

    }

    public Ability (Integer id, String name, String abbreviation) {
        this (id, name, abbreviation, null, null);
    }

    public Ability (Integer id, String name, String abbreviation, Integer rank, Boolean proficient) {
        this.id = id;
        this.name = name;
        this.abbreviation = abbreviation;
        this.rank = rank;
        this.proficient = proficient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Boolean getProficient() {
        return proficient;
    }

    public void setProficient(Boolean proficient) {
        this.proficient = proficient;
    }
}
