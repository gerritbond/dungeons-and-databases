package com.udig.dungeons_and_databases.campaign_api.models;

import com.udig.dungeons_and_databases.campaign_api.enums.Alignment;
import com.udig.dungeons_and_databases.campaign_api.enums.Size;
import com.udig.dungeons_and_databases.campaign_api.enums.Type;

public class Creature {
    private Integer id;
    private String creatureName;
    private Alignment alignment;
    private Size size;
    private Type type;
    private Integer armorClass;
    private Integer hitPoints;
    private Integer hitDice;
    private Integer speed;
    private Double challengeRating;
    private Integer experienceReward;

    public Creature () {

    }

    public Creature (Integer id, String creatureName, Alignment alignment,
                     Size size, Type type, Integer armorClass, Integer hitPoints,
                     Integer hitDice, Integer speed, Double challengeRating,
                     Integer experienceReward) {
        this.id = id;
        this.creatureName = creatureName;
        this.alignment = alignment;
        this.size = size;
        this.type = type;
        this.armorClass = armorClass;
        this.hitPoints = hitPoints;
        this.hitDice = hitDice;
        this.speed = speed;
        this.challengeRating = challengeRating;
        this.experienceReward = experienceReward;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatureName() {
        return creatureName;
    }

    public void setCreatureName(String creatureName) {
        this.creatureName = creatureName;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Integer getArmorClass() {
        return armorClass;
    }

    public void setArmorClass(Integer armorClass) {
        this.armorClass = armorClass;
    }

    public Integer getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(Integer hitPoints) {
        this.hitPoints = hitPoints;
    }

    public Integer getHitDice() {
        return hitDice;
    }

    public void setHitDice(Integer hitDice) {
        this.hitDice = hitDice;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Double getChallengeRating() {
        return challengeRating;
    }

    public void setChallengeRating(Double challengeRating) {
        this.challengeRating = challengeRating;
    }

    public Integer getExperienceReward() {
        return experienceReward;
    }

    public void setExperienceReward(Integer experienceReward) {
        this.experienceReward = experienceReward;
    }
}
