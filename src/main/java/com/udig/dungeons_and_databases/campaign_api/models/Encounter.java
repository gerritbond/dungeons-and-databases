package com.udig.dungeons_and_databases.campaign_api.models;

public class Encounter {
    private Integer id;
    private Double challengeRating;
    private Integer experienceReward;
    private Integer terrain;

    public Encounter () {

    }

    public Encounter (Integer id, Double challengeRating, Integer experienceReward, Integer terrain) {
        this.id = id;
        this.challengeRating = challengeRating;
        this.experienceReward = experienceReward;
        this.terrain = terrain;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getChallengeRating() {
        return challengeRating;
    }

    public void setChallengeRating(Double challengeRating) {
        this.challengeRating = challengeRating;
    }

    public Integer getExperienceReward() {
        return experienceReward;
    }

    public void setExperienceReward(Integer experienceReward) {
        this.experienceReward = experienceReward;
    }

    public Integer getTerrain() {
        return terrain;
    }

    public void setTerrain(Integer terrain) {
        this.terrain = terrain;
    }
}
