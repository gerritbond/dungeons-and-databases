package com.udig.dungeons_and_databases.campaign_api.models;

public class Spell {
    private Integer spellId;
    private String spellName;
    private String castingTime;
    private String components;
    private String description;
    private String duration;
    private Integer level;
    private String range;
    private String school;

    public Spell(final Integer spellId,
                 final String spellName,
                 final String castingTime,
                 final String components,
                 final String description,
                 final String duration,
                 final Integer level,
                 final String range,
                 final String school) {
        this.spellId = spellId;
        this.spellName = spellName;
        this.castingTime = castingTime;
        this.components = components;
        this.description = description;
        this.duration = duration;
        this.level = level;
        this.range = range;
        this.school = school;
    }

    public Spell() {

    }

    public Integer getSpellId() {
        return spellId;
    }

    public void setSpellId(final Integer spellId) {
        this.spellId = spellId;
    }

    public String getSpellName() {
        return spellName;
    }

    public void setSpellName (final String spellName) {
        this.spellName = spellName;
    }

    public String getCastingTime() {
        return castingTime;
    }

    public void setCastingTime (final String castingTime) {
        this.castingTime = castingTime;
    }

    public String getComponents() {
        return components;
    }

    public void setComponents (final String components) {
        this.components = components;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription (final String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration (final String duration) {
        this.duration = duration;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel (final Integer level) {
        this.level = level;
    }

    public String getRange() {
        return range;
    }

    public void setRange (final String range) {
        this.range = range;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool (final String school) {
        this.school = school;
    }
}
