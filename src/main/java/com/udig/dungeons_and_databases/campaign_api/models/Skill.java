package com.udig.dungeons_and_databases.campaign_api.models;

public class Skill {
    private Integer id;
    private String name;
    private Ability ability;

    public Skill () {

    }

    public Skill (Integer id, String name, Ability ability) {
        this.id = id;
        this.name = name;
        this.ability = ability;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }
}
