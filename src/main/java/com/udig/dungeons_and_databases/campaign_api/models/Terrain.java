package com.udig.dungeons_and_databases.campaign_api.models;

public class Terrain {
    private Integer id;
    private String terrainName;
    private String description;

    public Terrain () {

    }

    public Terrain (final Integer id, final String terrainName, final String description) {
        this.id = id;
        this.terrainName = terrainName;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTerrainName() {
        return terrainName;
    }

    public void setTerrainName(String terrainName) {
        this.terrainName = terrainName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
