package com.udig.dungeons_and_databases.campaign_api.models;

public class Player {
    private Integer id;
    private String firstName;
    private String lastName;

    public Player () {

    }

    public Player (Integer id) {
        this.id = id;
    }

    public Player (Integer id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Player (String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName (final String lastName) {
        this.lastName = lastName;
    }
}
