package com.udig.dungeons_and_databases.campaign_api.models;

public class Feature {
    private Integer id;
    private String name;
    private String description;
    private Skill skill;

    public Feature () {

    }

    public Feature (Integer id, String name, String description, Skill skill) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.skill = skill;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }
}
