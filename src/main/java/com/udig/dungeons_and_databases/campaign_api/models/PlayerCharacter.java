package com.udig.dungeons_and_databases.campaign_api.models;

import com.udig.dungeons_and_databases.campaign_api.enums.Alignment;

public class PlayerCharacter {
    private Integer id;
    private String name;
    private String background;
    private String backstory;
    private Alignment alignment;
    private Integer experience;
    private Integer armorClass;
    private Boolean inspiration;
    private Integer proficiencyBonus;
    private Integer age;
    private Integer height;
    private Integer weight;
    private String eyes;
    private String skin;
    private String hair;
    private Integer hitpoints;
    private Integer currentHitpoints;
    private Integer temporaryHitpoints;
    private String personalityTraits;
    private String ideals;
    private String bonds;
    private String flaws;
    private Integer race;
    private Integer player;
    private Integer party;

    public PlayerCharacter () {

    }

    public PlayerCharacter(Integer id, String name, String background, String backstory, Alignment alignment, Integer experience, Integer armorClass, Boolean inspiration, Integer proficiencyBonus, Integer age, Integer height, Integer weight, String eyes, String skin, String hair, Integer hitpoints, Integer currentHitpoints, Integer temporaryHitpoints, String personalityTraits, String ideals, String bonds, String flaws, Integer race, Integer player, Integer party) {
        this.id = id;
        this.name = name;
        this.background = background;
        this.backstory = backstory;
        this.alignment = alignment;
        this.experience = experience;
        this.armorClass = armorClass;
        this.inspiration = inspiration;
        this.proficiencyBonus = proficiencyBonus;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.eyes = eyes;
        this.skin = skin;
        this.hair = hair;
        this.hitpoints = hitpoints;
        this.currentHitpoints = currentHitpoints;
        this.temporaryHitpoints = temporaryHitpoints;
        this.personalityTraits = personalityTraits;
        this.ideals = ideals;
        this.bonds = bonds;
        this.flaws = flaws;
        this.race = race;
        this.player = player;
        this.party = party;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getBackstory() {
        return backstory;
    }

    public void setBackstory(String backstory) {
        this.backstory = backstory;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Boolean getInspiration() {
        return inspiration;
    }

    public void setInspiration(Boolean inspiration) {
        this.inspiration = inspiration;
    }

    public Integer getProficiencyBonus() {
        return proficiencyBonus;
    }

    public void setProficiencyBonus(Integer proficiencyBonus) {
        this.proficiencyBonus = proficiencyBonus;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getEyes() {
        return eyes;
    }

    public void setEyes(String eyes) {
        this.eyes = eyes;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getHair() {
        return hair;
    }

    public void setHair(String hair) {
        this.hair = hair;
    }

    public Integer getHitpoints() {
        return hitpoints;
    }

    public void setHitpoints(Integer hitpoints) {
        this.hitpoints = hitpoints;
    }

    public Integer getCurrentHitpoints() {
        return currentHitpoints;
    }

    public void setCurrentHitpoints(Integer currentHitpoints) {
        this.currentHitpoints = currentHitpoints;
    }

    public Integer getTemporaryHitpoints() {
        return temporaryHitpoints;
    }

    public void setTemporaryHitpoints(Integer temporaryHitpoints) {
        this.temporaryHitpoints = temporaryHitpoints;
    }

    public String getPersonalityTraits() {
        return personalityTraits;
    }

    public void setPersonalityTraits(String personalityTraits) {
        this.personalityTraits = personalityTraits;
    }

    public String getIdeals() {
        return ideals;
    }

    public void setIdeals(String ideals) {
        this.ideals = ideals;
    }

    public String getBonds() {
        return bonds;
    }

    public void setBonds(String bonds) {
        this.bonds = bonds;
    }

    public String getFlaws() {
        return flaws;
    }

    public void setFlaws(String flaws) {
        this.flaws = flaws;
    }

    public Integer getRace() {
        return race;
    }

    public void setRace(Integer race) {
        this.race = race;
    }

    public Integer getPlayer() {
        return player;
    }

    public void setPlayer(Integer player) {
        this.player = player;
    }

    public Integer getParty() {return party;}

    public void setParty(Integer party) { this.party = party; }

    public Integer getArmorClass() {
        return armorClass;
    }

    public void setArmorClass(Integer armorClass) {
        this.armorClass = armorClass;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }
}
