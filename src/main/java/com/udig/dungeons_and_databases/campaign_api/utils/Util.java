package com.udig.dungeons_and_databases.campaign_api.utils;

import org.jooq.Field;

import java.util.stream.Stream;


public class Util {
    public static Field[] mergeFields (Field [] ... arrays) {
        return Stream.of(arrays).flatMap(Stream::of).toArray(Field[]::new);
    }
}
