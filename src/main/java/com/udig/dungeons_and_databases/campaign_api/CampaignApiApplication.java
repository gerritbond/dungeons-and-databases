package com.udig.dungeons_and_databases.campaign_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "com.udig.dungeons_and_databases.campaign_api")
@SpringBootApplication
public class CampaignApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(CampaignApiApplication.class, args);
	}
}
