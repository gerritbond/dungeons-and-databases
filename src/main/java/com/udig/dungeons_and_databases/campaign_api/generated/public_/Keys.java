/*
 * This file is generated by jOOQ.
 */
package com.udig.dungeons_and_databases.campaign_api.generated.public_;


import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.*;
import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.*;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.UniqueKey;
import org.jooq.impl.Internal;

import javax.annotation.Generated;


/**
 * A class modelling foreign key relationships and constraints of tables of 
 * the <code>public</code> schema.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------

    public static final Identity<AbilityRecord, Integer> IDENTITY_ABILITY = Identities0.IDENTITY_ABILITY;
    public static final Identity<ClazzRecord, Integer> IDENTITY_CLAZZ = Identities0.IDENTITY_CLAZZ;
    public static final Identity<CreatureRecord, Integer> IDENTITY_CREATURE = Identities0.IDENTITY_CREATURE;
    public static final Identity<EncounterRecord, Integer> IDENTITY_ENCOUNTER = Identities0.IDENTITY_ENCOUNTER;
    public static final Identity<FeatureRecord, Integer> IDENTITY_FEATURE = Identities0.IDENTITY_FEATURE;
    public static final Identity<ItemRecord, Integer> IDENTITY_ITEM = Identities0.IDENTITY_ITEM;
    public static final Identity<OrganizationRecord, Integer> IDENTITY_ORGANIZATION = Identities0.IDENTITY_ORGANIZATION;
    public static final Identity<PartyRecord, Integer> IDENTITY_PARTY = Identities0.IDENTITY_PARTY;
    public static final Identity<PlayerRecord, Integer> IDENTITY_PLAYER = Identities0.IDENTITY_PLAYER;
    public static final Identity<PlayerCharacterRecord, Integer> IDENTITY_PLAYER_CHARACTER = Identities0.IDENTITY_PLAYER_CHARACTER;
    public static final Identity<RaceRecord, Integer> IDENTITY_RACE = Identities0.IDENTITY_RACE;
    public static final Identity<SkillRecord, Integer> IDENTITY_SKILL = Identities0.IDENTITY_SKILL;
    public static final Identity<SpellRecord, Integer> IDENTITY_SPELL = Identities0.IDENTITY_SPELL;
    public static final Identity<TerrainRecord, Integer> IDENTITY_TERRAIN = Identities0.IDENTITY_TERRAIN;

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<AbilityRecord> ABILITY_PKEY = UniqueKeys0.ABILITY_PKEY;
    public static final UniqueKey<AbilityRecord> ABILITY_ABILITY_NAME_KEY = UniqueKeys0.ABILITY_ABILITY_NAME_KEY;
    public static final UniqueKey<ClazzRecord> CLAZZ_PKEY = UniqueKeys0.CLAZZ_PKEY;
    public static final UniqueKey<ClazzRecord> CLAZZ_CLAZZ_NAME_KEY = UniqueKeys0.CLAZZ_CLAZZ_NAME_KEY;
    public static final UniqueKey<CreatureRecord> CREATURE_PKEY = UniqueKeys0.CREATURE_PKEY;
    public static final UniqueKey<CreatureAbilityXrefRecord> CREATURE_ABILITY_XREF_PKEY = UniqueKeys0.CREATURE_ABILITY_XREF_PKEY;
    public static final UniqueKey<CreatureEncounterXrefRecord> CREATURE_ENCOUNTER_XREF_PKEY = UniqueKeys0.CREATURE_ENCOUNTER_XREF_PKEY;
    public static final UniqueKey<CreatureSpellXrefRecord> CREATURE_SPELL_XREF_PKEY = UniqueKeys0.CREATURE_SPELL_XREF_PKEY;
    public static final UniqueKey<CreatureTerrainXrefRecord> CREATURE_TERRAIN_XREF_PKEY = UniqueKeys0.CREATURE_TERRAIN_XREF_PKEY;
    public static final UniqueKey<EncounterRecord> ENCOUNTER_PKEY = UniqueKeys0.ENCOUNTER_PKEY;
    public static final UniqueKey<FeatureRecord> FEATURE_PKEY = UniqueKeys0.FEATURE_PKEY;
    public static final UniqueKey<ItemRecord> ITEM_PKEY = UniqueKeys0.ITEM_PKEY;
    public static final UniqueKey<OrganizationRecord> ORGANIZATION_PKEY = UniqueKeys0.ORGANIZATION_PKEY;
    public static final UniqueKey<PartyRecord> PARTY_PKEY = UniqueKeys0.PARTY_PKEY;
    public static final UniqueKey<PlayerRecord> PLAYER_PKEY = UniqueKeys0.PLAYER_PKEY;
    public static final UniqueKey<PlayerCharacterRecord> PLAYER_CHARACTER_PKEY = UniqueKeys0.PLAYER_CHARACTER_PKEY;
    public static final UniqueKey<PlayerCharacterAbilityXrefRecord> PC_A_XREF_UNIQUE = UniqueKeys0.PC_A_XREF_UNIQUE;
    public static final UniqueKey<PlayerCharacterFeatureXrefRecord> PC_F_XREF_UNIQUE = UniqueKeys0.PC_F_XREF_UNIQUE;
    public static final UniqueKey<PlayerCharacterItemXrefRecord> PC_I_XREF_UNIQUE = UniqueKeys0.PC_I_XREF_UNIQUE;
    public static final UniqueKey<PlayerCharacterOrganizationXrefRecord> PC_O_XREF_UNIQUE = UniqueKeys0.PC_O_XREF_UNIQUE;
    public static final UniqueKey<PlayerCharacterSkillXrefRecord> PC_S_XREF_UNIQUE = UniqueKeys0.PC_S_XREF_UNIQUE;
    public static final UniqueKey<RaceRecord> RACE_PKEY = UniqueKeys0.RACE_PKEY;
    public static final UniqueKey<RaceAbilityXrefRecord> R_A_XREF_UNIQUE = UniqueKeys0.R_A_XREF_UNIQUE;
    public static final UniqueKey<RaceSkillXrefRecord> R_S_XREF_UNIQUE = UniqueKeys0.R_S_XREF_UNIQUE;
    public static final UniqueKey<SkillRecord> SKILL_PKEY = UniqueKeys0.SKILL_PKEY;
    public static final UniqueKey<SkillRecord> SKILL_SKILL_NAME_KEY = UniqueKeys0.SKILL_SKILL_NAME_KEY;
    public static final UniqueKey<SpellRecord> SPELL_PKEY = UniqueKeys0.SPELL_PKEY;
    public static final UniqueKey<SpellRecord> SPELL_SPELL_NAME_KEY = UniqueKeys0.SPELL_SPELL_NAME_KEY;
    public static final UniqueKey<TerrainRecord> TERRAIN_PKEY = UniqueKeys0.TERRAIN_PKEY;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------

    public static final ForeignKey<CreatureAbilityXrefRecord, AbilityRecord> CREATURE_ABILITY_XREF__CREATURE_ABILITY_XREF_ABILITY_FKEY = ForeignKeys0.CREATURE_ABILITY_XREF__CREATURE_ABILITY_XREF_ABILITY_FKEY;
    public static final ForeignKey<CreatureAbilityXrefRecord, CreatureRecord> CREATURE_ABILITY_XREF__CREATURE_ABILITY_XREF_CREATURE_FKEY = ForeignKeys0.CREATURE_ABILITY_XREF__CREATURE_ABILITY_XREF_CREATURE_FKEY;
    public static final ForeignKey<CreatureEncounterXrefRecord, CreatureRecord> CREATURE_ENCOUNTER_XREF__CREATURE_ENCOUNTER_XREF_CREATURE_FKEY = ForeignKeys0.CREATURE_ENCOUNTER_XREF__CREATURE_ENCOUNTER_XREF_CREATURE_FKEY;
    public static final ForeignKey<CreatureEncounterXrefRecord, EncounterRecord> CREATURE_ENCOUNTER_XREF__CREATURE_ENCOUNTER_XREF_ENCOUNTER_FKEY = ForeignKeys0.CREATURE_ENCOUNTER_XREF__CREATURE_ENCOUNTER_XREF_ENCOUNTER_FKEY;
    public static final ForeignKey<CreatureSpellXrefRecord, SpellRecord> CREATURE_SPELL_XREF__CREATURE_SPELL_XREF_SPELL_FKEY = ForeignKeys0.CREATURE_SPELL_XREF__CREATURE_SPELL_XREF_SPELL_FKEY;
    public static final ForeignKey<CreatureSpellXrefRecord, CreatureRecord> CREATURE_SPELL_XREF__CREATURE_SPELL_XREF_CREATURE_FKEY = ForeignKeys0.CREATURE_SPELL_XREF__CREATURE_SPELL_XREF_CREATURE_FKEY;
    public static final ForeignKey<CreatureTerrainXrefRecord, TerrainRecord> CREATURE_TERRAIN_XREF__CREATURE_TERRAIN_XREF_TERRAIN_FKEY = ForeignKeys0.CREATURE_TERRAIN_XREF__CREATURE_TERRAIN_XREF_TERRAIN_FKEY;
    public static final ForeignKey<CreatureTerrainXrefRecord, CreatureRecord> CREATURE_TERRAIN_XREF__CREATURE_TERRAIN_XREF_CREATURE_FKEY = ForeignKeys0.CREATURE_TERRAIN_XREF__CREATURE_TERRAIN_XREF_CREATURE_FKEY;
    public static final ForeignKey<EncounterRecord, TerrainRecord> ENCOUNTER__ENCOUNTER_TERRAIN_FKEY = ForeignKeys0.ENCOUNTER__ENCOUNTER_TERRAIN_FKEY;
    public static final ForeignKey<FeatureRecord, SkillRecord> FEATURE__FEATURE_SKILL_FKEY = ForeignKeys0.FEATURE__FEATURE_SKILL_FKEY;
    public static final ForeignKey<PartyRecord, PlayerRecord> PARTY__PARTY_GAME_MASTER_FKEY = ForeignKeys0.PARTY__PARTY_GAME_MASTER_FKEY;
    public static final ForeignKey<PlayerCharacterRecord, RaceRecord> PLAYER_CHARACTER__PLAYER_CHARACTER_RACE_FKEY = ForeignKeys0.PLAYER_CHARACTER__PLAYER_CHARACTER_RACE_FKEY;
    public static final ForeignKey<PlayerCharacterRecord, PlayerRecord> PLAYER_CHARACTER__PLAYER_CHARACTER_PLAYER_FKEY = ForeignKeys0.PLAYER_CHARACTER__PLAYER_CHARACTER_PLAYER_FKEY;
    public static final ForeignKey<PlayerCharacterRecord, PartyRecord> PLAYER_CHARACTER__PLAYER_CHARACTER_PARTY_FKEY = ForeignKeys0.PLAYER_CHARACTER__PLAYER_CHARACTER_PARTY_FKEY;
    public static final ForeignKey<PlayerCharacterAbilityXrefRecord, AbilityRecord> PLAYER_CHARACTER_ABILITY_XREF__PLAYER_CHARACTER_ABILITY_XREF_ABILITY_FKEY = ForeignKeys0.PLAYER_CHARACTER_ABILITY_XREF__PLAYER_CHARACTER_ABILITY_XREF_ABILITY_FKEY;
    public static final ForeignKey<PlayerCharacterFeatureXrefRecord, FeatureRecord> PLAYER_CHARACTER_FEATURE_XREF__PLAYER_CHARACTER_FEATURE_XREF_FEATURE_FKEY = ForeignKeys0.PLAYER_CHARACTER_FEATURE_XREF__PLAYER_CHARACTER_FEATURE_XREF_FEATURE_FKEY;
    public static final ForeignKey<PlayerCharacterItemXrefRecord, ItemRecord> PLAYER_CHARACTER_ITEM_XREF__PLAYER_CHARACTER_ITEM_XREF_ITEM_FKEY = ForeignKeys0.PLAYER_CHARACTER_ITEM_XREF__PLAYER_CHARACTER_ITEM_XREF_ITEM_FKEY;
    public static final ForeignKey<PlayerCharacterOrganizationXrefRecord, OrganizationRecord> PLAYER_CHARACTER_ORGANIZATION_XREF__PLAYER_CHARACTER_ORGANIZATION_XREF_ORGANIZATION_FKEY = ForeignKeys0.PLAYER_CHARACTER_ORGANIZATION_XREF__PLAYER_CHARACTER_ORGANIZATION_XREF_ORGANIZATION_FKEY;
    public static final ForeignKey<PlayerCharacterSkillXrefRecord, SkillRecord> PLAYER_CHARACTER_SKILL_XREF__PLAYER_CHARACTER_SKILL_XREF_SKILL_FKEY = ForeignKeys0.PLAYER_CHARACTER_SKILL_XREF__PLAYER_CHARACTER_SKILL_XREF_SKILL_FKEY;
    public static final ForeignKey<PlayerCharacterSpellXrefRecord, PlayerCharacterRecord> PLAYER_CHARACTER_SPELL_XREF__PLAYER_CHARACTER_SPELL_XREF_PLAYER_CHARACTER_FKEY = ForeignKeys0.PLAYER_CHARACTER_SPELL_XREF__PLAYER_CHARACTER_SPELL_XREF_PLAYER_CHARACTER_FKEY;
    public static final ForeignKey<PlayerCharacterSpellXrefRecord, SpellRecord> PLAYER_CHARACTER_SPELL_XREF__PLAYER_CHARACTER_SPELL_XREF_SPELL_FKEY = ForeignKeys0.PLAYER_CHARACTER_SPELL_XREF__PLAYER_CHARACTER_SPELL_XREF_SPELL_FKEY;
    public static final ForeignKey<RaceAbilityXrefRecord, AbilityRecord> RACE_ABILITY_XREF__RACE_ABILITY_XREF_ABILITY_FKEY = ForeignKeys0.RACE_ABILITY_XREF__RACE_ABILITY_XREF_ABILITY_FKEY;
    public static final ForeignKey<RaceAbilityXrefRecord, RaceRecord> RACE_ABILITY_XREF__RACE_ABILITY_XREF_RACE_FKEY = ForeignKeys0.RACE_ABILITY_XREF__RACE_ABILITY_XREF_RACE_FKEY;
    public static final ForeignKey<RaceSkillXrefRecord, RaceRecord> RACE_SKILL_XREF__RACE_SKILL_XREF_RACE_FKEY = ForeignKeys0.RACE_SKILL_XREF__RACE_SKILL_XREF_RACE_FKEY;
    public static final ForeignKey<RaceSkillXrefRecord, SkillRecord> RACE_SKILL_XREF__RACE_SKILL_XREF_SKILL_FKEY = ForeignKeys0.RACE_SKILL_XREF__RACE_SKILL_XREF_SKILL_FKEY;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Identities0 {
        public static Identity<AbilityRecord, Integer> IDENTITY_ABILITY = Internal.createIdentity(Ability.ABILITY, Ability.ABILITY.ABILITY_ID);
        public static Identity<ClazzRecord, Integer> IDENTITY_CLAZZ = Internal.createIdentity(Clazz.CLAZZ, Clazz.CLAZZ.CLAZZ_ID);
        public static Identity<CreatureRecord, Integer> IDENTITY_CREATURE = Internal.createIdentity(Creature.CREATURE, Creature.CREATURE.CREATURE_ID);
        public static Identity<EncounterRecord, Integer> IDENTITY_ENCOUNTER = Internal.createIdentity(Encounter.ENCOUNTER, Encounter.ENCOUNTER.ENCOUNTER_ID);
        public static Identity<FeatureRecord, Integer> IDENTITY_FEATURE = Internal.createIdentity(Feature.FEATURE, Feature.FEATURE.FEATURE_ID);
        public static Identity<ItemRecord, Integer> IDENTITY_ITEM = Internal.createIdentity(Item.ITEM, Item.ITEM.ITEM_ID);
        public static Identity<OrganizationRecord, Integer> IDENTITY_ORGANIZATION = Internal.createIdentity(Organization.ORGANIZATION, Organization.ORGANIZATION.ORGANIZATION_ID);
        public static Identity<PartyRecord, Integer> IDENTITY_PARTY = Internal.createIdentity(Party.PARTY, Party.PARTY.PARTY_ID);
        public static Identity<PlayerRecord, Integer> IDENTITY_PLAYER = Internal.createIdentity(Player.PLAYER, Player.PLAYER.PLAYER_ID);
        public static Identity<PlayerCharacterRecord, Integer> IDENTITY_PLAYER_CHARACTER = Internal.createIdentity(PlayerCharacter.PLAYER_CHARACTER, PlayerCharacter.PLAYER_CHARACTER.CHARACTER_ID);
        public static Identity<RaceRecord, Integer> IDENTITY_RACE = Internal.createIdentity(Race.RACE, Race.RACE.RACE_ID);
        public static Identity<SkillRecord, Integer> IDENTITY_SKILL = Internal.createIdentity(Skill.SKILL, Skill.SKILL.SKILL_ID);
        public static Identity<SpellRecord, Integer> IDENTITY_SPELL = Internal.createIdentity(Spell.SPELL, Spell.SPELL.SPELL_ID);
        public static Identity<TerrainRecord, Integer> IDENTITY_TERRAIN = Internal.createIdentity(Terrain.TERRAIN, Terrain.TERRAIN.TERRAIN_ID);
    }

    private static class UniqueKeys0 {
        public static final UniqueKey<AbilityRecord> ABILITY_PKEY = Internal.createUniqueKey(Ability.ABILITY, "ability_pkey", Ability.ABILITY.ABILITY_ID);
        public static final UniqueKey<AbilityRecord> ABILITY_ABILITY_NAME_KEY = Internal.createUniqueKey(Ability.ABILITY, "ability_ability_name_key", Ability.ABILITY.ABILITY_NAME);
        public static final UniqueKey<ClazzRecord> CLAZZ_PKEY = Internal.createUniqueKey(Clazz.CLAZZ, "clazz_pkey", Clazz.CLAZZ.CLAZZ_ID);
        public static final UniqueKey<ClazzRecord> CLAZZ_CLAZZ_NAME_KEY = Internal.createUniqueKey(Clazz.CLAZZ, "clazz_clazz_name_key", Clazz.CLAZZ.CLAZZ_NAME);
        public static final UniqueKey<CreatureRecord> CREATURE_PKEY = Internal.createUniqueKey(Creature.CREATURE, "creature_pkey", Creature.CREATURE.CREATURE_ID);
        public static final UniqueKey<CreatureAbilityXrefRecord> CREATURE_ABILITY_XREF_PKEY = Internal.createUniqueKey(CreatureAbilityXref.CREATURE_ABILITY_XREF, "creature_ability_xref_pkey", CreatureAbilityXref.CREATURE_ABILITY_XREF.ABILITY, CreatureAbilityXref.CREATURE_ABILITY_XREF.CREATURE);
        public static final UniqueKey<CreatureEncounterXrefRecord> CREATURE_ENCOUNTER_XREF_PKEY = Internal.createUniqueKey(CreatureEncounterXref.CREATURE_ENCOUNTER_XREF, "creature_encounter_xref_pkey", CreatureEncounterXref.CREATURE_ENCOUNTER_XREF.CREATURE, CreatureEncounterXref.CREATURE_ENCOUNTER_XREF.ENCOUNTER);
        public static final UniqueKey<CreatureSpellXrefRecord> CREATURE_SPELL_XREF_PKEY = Internal.createUniqueKey(CreatureSpellXref.CREATURE_SPELL_XREF, "creature_spell_xref_pkey", CreatureSpellXref.CREATURE_SPELL_XREF.SPELL, CreatureSpellXref.CREATURE_SPELL_XREF.CREATURE);
        public static final UniqueKey<CreatureTerrainXrefRecord> CREATURE_TERRAIN_XREF_PKEY = Internal.createUniqueKey(CreatureTerrainXref.CREATURE_TERRAIN_XREF, "creature_terrain_xref_pkey", CreatureTerrainXref.CREATURE_TERRAIN_XREF.TERRAIN, CreatureTerrainXref.CREATURE_TERRAIN_XREF.CREATURE);
        public static final UniqueKey<EncounterRecord> ENCOUNTER_PKEY = Internal.createUniqueKey(Encounter.ENCOUNTER, "encounter_pkey", Encounter.ENCOUNTER.ENCOUNTER_ID);
        public static final UniqueKey<FeatureRecord> FEATURE_PKEY = Internal.createUniqueKey(Feature.FEATURE, "feature_pkey", Feature.FEATURE.FEATURE_ID);
        public static final UniqueKey<ItemRecord> ITEM_PKEY = Internal.createUniqueKey(Item.ITEM, "item_pkey", Item.ITEM.ITEM_ID);
        public static final UniqueKey<OrganizationRecord> ORGANIZATION_PKEY = Internal.createUniqueKey(Organization.ORGANIZATION, "organization_pkey", Organization.ORGANIZATION.ORGANIZATION_ID);
        public static final UniqueKey<PartyRecord> PARTY_PKEY = Internal.createUniqueKey(Party.PARTY, "party_pkey", Party.PARTY.PARTY_ID);
        public static final UniqueKey<PlayerRecord> PLAYER_PKEY = Internal.createUniqueKey(Player.PLAYER, "player_pkey", Player.PLAYER.PLAYER_ID);
        public static final UniqueKey<PlayerCharacterRecord> PLAYER_CHARACTER_PKEY = Internal.createUniqueKey(PlayerCharacter.PLAYER_CHARACTER, "player_character_pkey", PlayerCharacter.PLAYER_CHARACTER.CHARACTER_ID);
        public static final UniqueKey<PlayerCharacterAbilityXrefRecord> PC_A_XREF_UNIQUE = Internal.createUniqueKey(PlayerCharacterAbilityXref.PLAYER_CHARACTER_ABILITY_XREF, "pc_a_xref_unique", PlayerCharacterAbilityXref.PLAYER_CHARACTER_ABILITY_XREF.PLAYER_CHARACTER, PlayerCharacterAbilityXref.PLAYER_CHARACTER_ABILITY_XREF.ABILITY);
        public static final UniqueKey<PlayerCharacterFeatureXrefRecord> PC_F_XREF_UNIQUE = Internal.createUniqueKey(PlayerCharacterFeatureXref.PLAYER_CHARACTER_FEATURE_XREF, "pc_f_xref_unique", PlayerCharacterFeatureXref.PLAYER_CHARACTER_FEATURE_XREF.FEATURE, PlayerCharacterFeatureXref.PLAYER_CHARACTER_FEATURE_XREF.PLAYER_CHARACTER);
        public static final UniqueKey<PlayerCharacterItemXrefRecord> PC_I_XREF_UNIQUE = Internal.createUniqueKey(PlayerCharacterItemXref.PLAYER_CHARACTER_ITEM_XREF, "pc_i_xref_unique", PlayerCharacterItemXref.PLAYER_CHARACTER_ITEM_XREF.ITEM, PlayerCharacterItemXref.PLAYER_CHARACTER_ITEM_XREF.PLAYER_CHARACTER);
        public static final UniqueKey<PlayerCharacterOrganizationXrefRecord> PC_O_XREF_UNIQUE = Internal.createUniqueKey(PlayerCharacterOrganizationXref.PLAYER_CHARACTER_ORGANIZATION_XREF, "pc_o_xref_unique", PlayerCharacterOrganizationXref.PLAYER_CHARACTER_ORGANIZATION_XREF.ORGANIZATION, PlayerCharacterOrganizationXref.PLAYER_CHARACTER_ORGANIZATION_XREF.PLAYER_CHARACTER);
        public static final UniqueKey<PlayerCharacterSkillXrefRecord> PC_S_XREF_UNIQUE = Internal.createUniqueKey(PlayerCharacterSkillXref.PLAYER_CHARACTER_SKILL_XREF, "pc_s_xref_unique", PlayerCharacterSkillXref.PLAYER_CHARACTER_SKILL_XREF.PLAYER_CHARACTER, PlayerCharacterSkillXref.PLAYER_CHARACTER_SKILL_XREF.SKILL);
        public static final UniqueKey<RaceRecord> RACE_PKEY = Internal.createUniqueKey(Race.RACE, "race_pkey", Race.RACE.RACE_ID);
        public static final UniqueKey<RaceAbilityXrefRecord> R_A_XREF_UNIQUE = Internal.createUniqueKey(RaceAbilityXref.RACE_ABILITY_XREF, "r_a_xref_unique", RaceAbilityXref.RACE_ABILITY_XREF.RACE, RaceAbilityXref.RACE_ABILITY_XREF.ABILITY);
        public static final UniqueKey<RaceSkillXrefRecord> R_S_XREF_UNIQUE = Internal.createUniqueKey(RaceSkillXref.RACE_SKILL_XREF, "r_s_xref_unique", RaceSkillXref.RACE_SKILL_XREF.RACE, RaceSkillXref.RACE_SKILL_XREF.SKILL);
        public static final UniqueKey<SkillRecord> SKILL_PKEY = Internal.createUniqueKey(Skill.SKILL, "skill_pkey", Skill.SKILL.SKILL_ID);
        public static final UniqueKey<SkillRecord> SKILL_SKILL_NAME_KEY = Internal.createUniqueKey(Skill.SKILL, "skill_skill_name_key", Skill.SKILL.SKILL_NAME);
        public static final UniqueKey<SpellRecord> SPELL_PKEY = Internal.createUniqueKey(Spell.SPELL, "spell_pkey", Spell.SPELL.SPELL_ID);
        public static final UniqueKey<SpellRecord> SPELL_SPELL_NAME_KEY = Internal.createUniqueKey(Spell.SPELL, "spell_spell_name_key", Spell.SPELL.SPELL_NAME);
        public static final UniqueKey<TerrainRecord> TERRAIN_PKEY = Internal.createUniqueKey(Terrain.TERRAIN, "terrain_pkey", Terrain.TERRAIN.TERRAIN_ID);
    }

    private static class ForeignKeys0 {
        public static final ForeignKey<CreatureAbilityXrefRecord, AbilityRecord> CREATURE_ABILITY_XREF__CREATURE_ABILITY_XREF_ABILITY_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.ABILITY_PKEY, CreatureAbilityXref.CREATURE_ABILITY_XREF, "creature_ability_xref__creature_ability_xref_ability_fkey", CreatureAbilityXref.CREATURE_ABILITY_XREF.ABILITY);
        public static final ForeignKey<CreatureAbilityXrefRecord, CreatureRecord> CREATURE_ABILITY_XREF__CREATURE_ABILITY_XREF_CREATURE_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.CREATURE_PKEY, CreatureAbilityXref.CREATURE_ABILITY_XREF, "creature_ability_xref__creature_ability_xref_creature_fkey", CreatureAbilityXref.CREATURE_ABILITY_XREF.CREATURE);
        public static final ForeignKey<CreatureEncounterXrefRecord, CreatureRecord> CREATURE_ENCOUNTER_XREF__CREATURE_ENCOUNTER_XREF_CREATURE_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.CREATURE_PKEY, CreatureEncounterXref.CREATURE_ENCOUNTER_XREF, "creature_encounter_xref__creature_encounter_xref_creature_fkey", CreatureEncounterXref.CREATURE_ENCOUNTER_XREF.CREATURE);
        public static final ForeignKey<CreatureEncounterXrefRecord, EncounterRecord> CREATURE_ENCOUNTER_XREF__CREATURE_ENCOUNTER_XREF_ENCOUNTER_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.ENCOUNTER_PKEY, CreatureEncounterXref.CREATURE_ENCOUNTER_XREF, "creature_encounter_xref__creature_encounter_xref_encounter_fkey", CreatureEncounterXref.CREATURE_ENCOUNTER_XREF.ENCOUNTER);
        public static final ForeignKey<CreatureSpellXrefRecord, SpellRecord> CREATURE_SPELL_XREF__CREATURE_SPELL_XREF_SPELL_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.SPELL_PKEY, CreatureSpellXref.CREATURE_SPELL_XREF, "creature_spell_xref__creature_spell_xref_spell_fkey", CreatureSpellXref.CREATURE_SPELL_XREF.SPELL);
        public static final ForeignKey<CreatureSpellXrefRecord, CreatureRecord> CREATURE_SPELL_XREF__CREATURE_SPELL_XREF_CREATURE_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.CREATURE_PKEY, CreatureSpellXref.CREATURE_SPELL_XREF, "creature_spell_xref__creature_spell_xref_creature_fkey", CreatureSpellXref.CREATURE_SPELL_XREF.CREATURE);
        public static final ForeignKey<CreatureTerrainXrefRecord, TerrainRecord> CREATURE_TERRAIN_XREF__CREATURE_TERRAIN_XREF_TERRAIN_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.TERRAIN_PKEY, CreatureTerrainXref.CREATURE_TERRAIN_XREF, "creature_terrain_xref__creature_terrain_xref_terrain_fkey", CreatureTerrainXref.CREATURE_TERRAIN_XREF.TERRAIN);
        public static final ForeignKey<CreatureTerrainXrefRecord, CreatureRecord> CREATURE_TERRAIN_XREF__CREATURE_TERRAIN_XREF_CREATURE_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.CREATURE_PKEY, CreatureTerrainXref.CREATURE_TERRAIN_XREF, "creature_terrain_xref__creature_terrain_xref_creature_fkey", CreatureTerrainXref.CREATURE_TERRAIN_XREF.CREATURE);
        public static final ForeignKey<EncounterRecord, TerrainRecord> ENCOUNTER__ENCOUNTER_TERRAIN_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.TERRAIN_PKEY, Encounter.ENCOUNTER, "encounter__encounter_terrain_fkey", Encounter.ENCOUNTER.TERRAIN);
        public static final ForeignKey<FeatureRecord, SkillRecord> FEATURE__FEATURE_SKILL_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.SKILL_PKEY, Feature.FEATURE, "feature__feature_skill_fkey", Feature.FEATURE.SKILL);
        public static final ForeignKey<PartyRecord, PlayerRecord> PARTY__PARTY_GAME_MASTER_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.PLAYER_PKEY, Party.PARTY, "party__party_game_master_fkey", Party.PARTY.GAME_MASTER);
        public static final ForeignKey<PlayerCharacterRecord, RaceRecord> PLAYER_CHARACTER__PLAYER_CHARACTER_RACE_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.RACE_PKEY, PlayerCharacter.PLAYER_CHARACTER, "player_character__player_character_race_fkey", PlayerCharacter.PLAYER_CHARACTER.RACE);
        public static final ForeignKey<PlayerCharacterRecord, PlayerRecord> PLAYER_CHARACTER__PLAYER_CHARACTER_PLAYER_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.PLAYER_PKEY, PlayerCharacter.PLAYER_CHARACTER, "player_character__player_character_player_fkey", PlayerCharacter.PLAYER_CHARACTER.PLAYER);
        public static final ForeignKey<PlayerCharacterRecord, PartyRecord> PLAYER_CHARACTER__PLAYER_CHARACTER_PARTY_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.PARTY_PKEY, PlayerCharacter.PLAYER_CHARACTER, "player_character__player_character_party_fkey", PlayerCharacter.PLAYER_CHARACTER.PARTY);
        public static final ForeignKey<PlayerCharacterAbilityXrefRecord, AbilityRecord> PLAYER_CHARACTER_ABILITY_XREF__PLAYER_CHARACTER_ABILITY_XREF_ABILITY_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.ABILITY_PKEY, PlayerCharacterAbilityXref.PLAYER_CHARACTER_ABILITY_XREF, "player_character_ability_xref__player_character_ability_xref_ability_fkey", PlayerCharacterAbilityXref.PLAYER_CHARACTER_ABILITY_XREF.ABILITY);
        public static final ForeignKey<PlayerCharacterFeatureXrefRecord, FeatureRecord> PLAYER_CHARACTER_FEATURE_XREF__PLAYER_CHARACTER_FEATURE_XREF_FEATURE_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.FEATURE_PKEY, PlayerCharacterFeatureXref.PLAYER_CHARACTER_FEATURE_XREF, "player_character_feature_xref__player_character_feature_xref_feature_fkey", PlayerCharacterFeatureXref.PLAYER_CHARACTER_FEATURE_XREF.FEATURE);
        public static final ForeignKey<PlayerCharacterItemXrefRecord, ItemRecord> PLAYER_CHARACTER_ITEM_XREF__PLAYER_CHARACTER_ITEM_XREF_ITEM_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.ITEM_PKEY, PlayerCharacterItemXref.PLAYER_CHARACTER_ITEM_XREF, "player_character_item_xref__player_character_item_xref_item_fkey", PlayerCharacterItemXref.PLAYER_CHARACTER_ITEM_XREF.ITEM);
        public static final ForeignKey<PlayerCharacterOrganizationXrefRecord, OrganizationRecord> PLAYER_CHARACTER_ORGANIZATION_XREF__PLAYER_CHARACTER_ORGANIZATION_XREF_ORGANIZATION_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.ORGANIZATION_PKEY, PlayerCharacterOrganizationXref.PLAYER_CHARACTER_ORGANIZATION_XREF, "player_character_organization_xref__player_character_organization_xref_organization_fkey", PlayerCharacterOrganizationXref.PLAYER_CHARACTER_ORGANIZATION_XREF.ORGANIZATION);
        public static final ForeignKey<PlayerCharacterSkillXrefRecord, SkillRecord> PLAYER_CHARACTER_SKILL_XREF__PLAYER_CHARACTER_SKILL_XREF_SKILL_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.SKILL_PKEY, PlayerCharacterSkillXref.PLAYER_CHARACTER_SKILL_XREF, "player_character_skill_xref__player_character_skill_xref_skill_fkey", PlayerCharacterSkillXref.PLAYER_CHARACTER_SKILL_XREF.SKILL);
        public static final ForeignKey<PlayerCharacterSpellXrefRecord, PlayerCharacterRecord> PLAYER_CHARACTER_SPELL_XREF__PLAYER_CHARACTER_SPELL_XREF_PLAYER_CHARACTER_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.PLAYER_CHARACTER_PKEY, PlayerCharacterSpellXref.PLAYER_CHARACTER_SPELL_XREF, "player_character_spell_xref__player_character_spell_xref_player_character_fkey", PlayerCharacterSpellXref.PLAYER_CHARACTER_SPELL_XREF.PLAYER_CHARACTER);
        public static final ForeignKey<PlayerCharacterSpellXrefRecord, SpellRecord> PLAYER_CHARACTER_SPELL_XREF__PLAYER_CHARACTER_SPELL_XREF_SPELL_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.SPELL_PKEY, PlayerCharacterSpellXref.PLAYER_CHARACTER_SPELL_XREF, "player_character_spell_xref__player_character_spell_xref_spell_fkey", PlayerCharacterSpellXref.PLAYER_CHARACTER_SPELL_XREF.SPELL);
        public static final ForeignKey<RaceAbilityXrefRecord, AbilityRecord> RACE_ABILITY_XREF__RACE_ABILITY_XREF_ABILITY_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.ABILITY_PKEY, RaceAbilityXref.RACE_ABILITY_XREF, "race_ability_xref__race_ability_xref_ability_fkey", RaceAbilityXref.RACE_ABILITY_XREF.ABILITY);
        public static final ForeignKey<RaceAbilityXrefRecord, RaceRecord> RACE_ABILITY_XREF__RACE_ABILITY_XREF_RACE_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.RACE_PKEY, RaceAbilityXref.RACE_ABILITY_XREF, "race_ability_xref__race_ability_xref_race_fkey", RaceAbilityXref.RACE_ABILITY_XREF.RACE);
        public static final ForeignKey<RaceSkillXrefRecord, RaceRecord> RACE_SKILL_XREF__RACE_SKILL_XREF_RACE_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.RACE_PKEY, RaceSkillXref.RACE_SKILL_XREF, "race_skill_xref__race_skill_xref_race_fkey", RaceSkillXref.RACE_SKILL_XREF.RACE);
        public static final ForeignKey<RaceSkillXrefRecord, SkillRecord> RACE_SKILL_XREF__RACE_SKILL_XREF_SKILL_FKEY = Internal.createForeignKey(com.udig.dungeons_and_databases.campaign_api.generated.public_.Keys.SKILL_PKEY, RaceSkillXref.RACE_SKILL_XREF, "race_skill_xref__race_skill_xref_skill_fkey", RaceSkillXref.RACE_SKILL_XREF.SKILL);
    }
}
