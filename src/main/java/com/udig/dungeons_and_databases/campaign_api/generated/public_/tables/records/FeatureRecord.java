/*
 * This file is generated by jOOQ.
 */
package com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records;


import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.Feature;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class FeatureRecord extends UpdatableRecordImpl<FeatureRecord> implements Record4<Integer, String, String, Integer> {

    private static final long serialVersionUID = 1878964405;

    /**
     * Setter for <code>public.feature.feature_id</code>.
     */
    public void setFeatureId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.feature.feature_id</code>.
     */
    public Integer getFeatureId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.feature.feature_name</code>.
     */
    public void setFeatureName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.feature.feature_name</code>.
     */
    public String getFeatureName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.feature.description</code>.
     */
    public void setDescription(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.feature.description</code>.
     */
    public String getDescription() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.feature.skill</code>.
     */
    public void setSkill(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.feature.skill</code>.
     */
    public Integer getSkill() {
        return (Integer) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer, String, String, Integer> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer, String, String, Integer> valuesRow() {
        return (Row4) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Feature.FEATURE.FEATURE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Feature.FEATURE.FEATURE_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Feature.FEATURE.DESCRIPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field4() {
        return Feature.FEATURE.SKILL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getFeatureId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getFeatureName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component4() {
        return getSkill();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getFeatureId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getFeatureName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value4() {
        return getSkill();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FeatureRecord value1(Integer value) {
        setFeatureId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FeatureRecord value2(String value) {
        setFeatureName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FeatureRecord value3(String value) {
        setDescription(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FeatureRecord value4(Integer value) {
        setSkill(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FeatureRecord values(Integer value1, String value2, String value3, Integer value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached FeatureRecord
     */
    public FeatureRecord() {
        super(Feature.FEATURE);
    }

    /**
     * Create a detached, initialised FeatureRecord
     */
    public FeatureRecord(Integer featureId, String featureName, String description, Integer skill) {
        super(Feature.FEATURE);

        set(0, featureId);
        set(1, featureName);
        set(2, description);
        set(3, skill);
    }
}
