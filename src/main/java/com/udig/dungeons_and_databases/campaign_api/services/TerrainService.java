package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.TerrainRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Terrain;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.TERRAIN;

@Service
public class TerrainService implements BaseService<Terrain> {
    private final static Logger LOGGER = LoggerFactory.getLogger(TerrainService.class);
    private final DSLContext context;

    @Autowired
    public TerrainService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Terrain>> find() {
        return new AbstractServiceResponse<List<Terrain>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(TERRAIN.fields())
                        .from(TERRAIN)
                        .fetch()
                        .into(Terrain.class);
            }
        };
    }


    public ServiceResponse<Terrain> find (final Integer id) {
        return new AbstractServiceResponse<Terrain>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(TERRAIN.fields())
                        .from(TERRAIN)
                        .where(TERRAIN.TERRAIN_ID.eq(id))
                        .fetchOne()
                        .into(Terrain.class);
            }
        };
    }

    public ServiceResponse<Terrain> create (final Terrain terrain) {
        return new AbstractServiceResponse<Terrain>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    TerrainRecord terrainRecord = context.newRecord(TERRAIN, terrain);
                    Integer updatedRecords = context.executeInsert(terrainRecord);

                    if(updatedRecords > 0)
                        response = terrain;
                    else
                        status = HttpStatus.BAD_REQUEST;

                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Terrain, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Terrain> update (final Terrain terrain) {
        return new AbstractServiceResponse<Terrain>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    TerrainRecord terrainRecord = context.newRecord(TERRAIN, terrain);
                    Integer updatedRecords = context.executeUpdate(terrainRecord, TERRAIN.TERRAIN_ID.eq(terrain.getId()));

                    if(updatedRecords > 0)
                        response = terrain;
                    else
                        status = HttpStatus.BAD_REQUEST;
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Terrain, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(TERRAIN).where(TERRAIN.TERRAIN_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

}
