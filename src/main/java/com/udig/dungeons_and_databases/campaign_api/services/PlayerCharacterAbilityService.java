package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.AbilityRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Ability;
import com.udig.dungeons_and_databases.campaign_api.utils.Util;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.ABILITY;
import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER_CHARACTER_ABILITY_XREF;

@Service
public class PlayerCharacterAbilityService implements NestedService<Ability> {
    private final static Logger LOGGER = LoggerFactory.getLogger(PlayerCharacterAbilityService.class);
    private final DSLContext context;

    @Autowired
    public PlayerCharacterAbilityService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Ability>> find(final Integer baseId) {
        return new AbstractServiceResponse<List<Ability>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(Util.mergeFields(ABILITY.fields(), PLAYER_CHARACTER_ABILITY_XREF.fields()))
                        .from(ABILITY)
                        .join(PLAYER_CHARACTER_ABILITY_XREF).on(PLAYER_CHARACTER_ABILITY_XREF.ABILITY.eq(ABILITY.ABILITY_ID))
                        .where(PLAYER_CHARACTER_ABILITY_XREF.PLAYER_CHARACTER.eq(baseId))
                        .fetch()
                        .into(Ability.class);
            }
        };
    }


    public ServiceResponse<Ability> find (final Integer baseId, final Integer id) {
        return new AbstractServiceResponse<Ability>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(Util.mergeFields(ABILITY.fields(), PLAYER_CHARACTER_ABILITY_XREF.fields()))
                        .from(ABILITY)
                        .join(PLAYER_CHARACTER_ABILITY_XREF).on(PLAYER_CHARACTER_ABILITY_XREF.ABILITY.eq(ABILITY.ABILITY_ID))
                        .where(PLAYER_CHARACTER_ABILITY_XREF.PLAYER_CHARACTER.eq(baseId)
                                .and(ABILITY.ABILITY_ID.eq(id)))
                        .fetchOne()
                        .into(Ability.class);
            }
        };
    }

    public ServiceResponse<Ability> create (final Integer baseId, final Ability ability) {
        return new AbstractServiceResponse<Ability>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {

                    if(ability.getId() == null) {
                        Record1<Integer> idRecord = context.select(ABILITY.ABILITY_ID).from(ABILITY)
                                .where(ABILITY.ABILITY_NAME.eq(ability.getName()))
                                .and(ABILITY.ABBREVIATION.eq(ability.getAbbreviation()))
                                .fetchOne();

                        if(idRecord != null)
                            ability.setId(idRecord.value1());
                    }

                    if(ability.getId() == null)
                        ability.setId(context.insertInto(ABILITY, ABILITY.ABILITY_NAME, ABILITY.ABBREVIATION)
                                .values(ability.getName(), ability.getAbbreviation())
                                .returning(ABILITY.ABILITY_ID)
                                .fetchOne()
                                .getAbilityId());

                    response = find(baseId,
                            context.insertInto(PLAYER_CHARACTER_ABILITY_XREF,
                            PLAYER_CHARACTER_ABILITY_XREF.ABILITY,
                            PLAYER_CHARACTER_ABILITY_XREF.PLAYER_CHARACTER,
                            PLAYER_CHARACTER_ABILITY_XREF.RANK,
                            PLAYER_CHARACTER_ABILITY_XREF.PROFICIENT)
                            .values(ability.getId(), baseId, ability.getRank(), ability.getProficient())
                            .returning(PLAYER_CHARACTER_ABILITY_XREF.ABILITY)
                            .fetchOne()
                            .getAbility()).getResponse();
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Ability, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Ability> update (final Integer baseId, final Ability ability) {
        return new AbstractServiceResponse<Ability>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    if(ability.getId() != null)
                        response = find(baseId, context.update(PLAYER_CHARACTER_ABILITY_XREF)
                                .set(PLAYER_CHARACTER_ABILITY_XREF.RANK, ability.getRank())
                                .set(PLAYER_CHARACTER_ABILITY_XREF.PROFICIENT, ability.getProficient())
                                .where(PLAYER_CHARACTER_ABILITY_XREF.PLAYER_CHARACTER.eq(baseId))
                                .and(PLAYER_CHARACTER_ABILITY_XREF.ABILITY.eq(ability.getId()))
                                .returning(PLAYER_CHARACTER_ABILITY_XREF.ABILITY)
                                .fetchOne().getAbility()).getResponse();

                    AbilityRecord abilityRecord = context.newRecord(ABILITY, ability);
                    response = ability;

                    if(abilityRecord.update(ABILITY.ABBREVIATION, ABILITY.ABILITY_NAME) > 0) {
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Ability, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }
}
