package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.ClazzRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Clazz;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.CLAZZ;

@Service
public class ClazzService implements BaseService<Clazz> {
    private final static Logger LOGGER = LoggerFactory.getLogger(ClazzService.class);
    private final DSLContext context;

    @Autowired
    public ClazzService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Clazz>> find() {
        return new AbstractServiceResponse<List<Clazz>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(CLAZZ.fields())
                        .from(CLAZZ)
                        .fetch()
                        .into(Clazz.class);
            }
        };
    }


    public ServiceResponse<Clazz> find (final Integer id) {
        return new AbstractServiceResponse<Clazz>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(CLAZZ.fields())
                        .from(CLAZZ)
                        .where(CLAZZ.CLAZZ_ID.eq(id))
                        .fetchOne()
                        .into(Clazz.class);
            }
        };
    }

    public ServiceResponse<Clazz> create (final Clazz clazz) {
        return new AbstractServiceResponse<Clazz>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    response = context.insertInto(CLAZZ, CLAZZ.CLAZZ_NAME, CLAZZ.DESCRIPTION)
                            .values(clazz.getName(), clazz.getDescription())
                            .returning()
                            .fetchOne()
                            .into(Clazz.class);
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Clazz, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Clazz> update (final Clazz clazz) {
        return new AbstractServiceResponse<Clazz>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    ClazzRecord abilityRecord = context.newRecord(CLAZZ, clazz);
                    response = clazz;

                    if(abilityRecord.update(CLAZZ.CLAZZ_NAME, CLAZZ.DESCRIPTION) > 0) {
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Clazz, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(CLAZZ).where(CLAZZ.CLAZZ_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }
}
