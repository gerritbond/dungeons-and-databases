package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.PlayerRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Player;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER;

@Service
public class PlayerService implements BaseService<Player> {
    private final static Logger LOGGER = LoggerFactory.getLogger(PlayerService.class);
    private final DSLContext context;

    @Autowired
    public PlayerService (DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Player>> find() {
        return new AbstractServiceResponse<List<Player>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(PLAYER.fields())
                        .from(PLAYER)
                        .fetch()
                        .into(Player.class);
            }
        };
    }

    public AbstractServiceResponse<List<Player>> findAll(List<Integer> playerIds) {
        return new AbstractServiceResponse<List<Player>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(PLAYER.fields())
                        .from(PLAYER)
                        .where(PLAYER.PLAYER_ID.in(playerIds))
                        .fetch()
                        .into(Player.class);
            }
        };
    }

    public ServiceResponse<Player> find (final Integer id) {
        return new AbstractServiceResponse<Player>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(PLAYER.fields())
                        .from(PLAYER)
                        .where(PLAYER.PLAYER_ID.eq(id))
                        .fetchOne()
                        .into(Player.class);
            }
        };
    }

    public ServiceResponse<Player> create (final Player player) {
        return new AbstractServiceResponse<Player>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    response = context.insertInto(PLAYER, PLAYER.FIRST_NAME, PLAYER.LAST_NAME)
                            .values(player.getFirstName(), player.getLastName())
                            .returning()
                            .fetchOne()
                            .into(Player.class);
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Player, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Player> update (final Player player) {
        return new AbstractServiceResponse<Player>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    PlayerRecord playerRecord = context.newRecord(PLAYER, player);
                    response = player;

                    if(playerRecord.update(PLAYER.FIRST_NAME, PLAYER.LAST_NAME) > 0) {
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Player, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(PLAYER).where(PLAYER.PLAYER_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

}
