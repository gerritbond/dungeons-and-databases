package com.udig.dungeons_and_databases.campaign_api.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractServiceResponse<T> implements ServiceResponse <T> {
    T response;
    HttpStatus status;

    AbstractServiceResponse() {
        execute();

        if (status == null)
            status = HttpStatus.NOT_IMPLEMENTED;
    }

    public final T getResponse () {
        return response;
    }

    public final ResponseEntity<T> toResponseEntity () {
        return new ResponseEntity<>(response, status);
    }

    public abstract void execute ();
}
