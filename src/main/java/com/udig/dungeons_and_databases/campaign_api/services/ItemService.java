package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.models.Item;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.ITEM;

@Service
public class ItemService implements BaseService<Item> {
    private final static Logger LOGGER = LoggerFactory.getLogger(ItemService.class);
    private final DSLContext context;

    @Autowired
    public ItemService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Item>> find() {
        return new AbstractServiceResponse<List<Item>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(ITEM.fields())
                        .from(ITEM)
                        .fetch()
                        .into(Item.class);
            }
        };
    }


    public ServiceResponse<Item> find (final Integer id) {
        return new AbstractServiceResponse<Item>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(ITEM.fields())
                        .from(ITEM)
                        .where(ITEM.ITEM_ID.eq(id))
                        .fetchOne()
                        .into(Item.class);
            }
        };
    }

    public ServiceResponse<Item> create (final Item item) {
        return new AbstractServiceResponse<Item>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {

                    if(item.getId() == null) {
                        Record1<Integer> idRecord = context.select(ITEM.ITEM_ID).from(ITEM)
                                .where(ITEM.ITEM_NAME.eq(item.getName()))
                                .fetchOne();

                        if(idRecord != null)
                            item.setId(idRecord.value1());
                    }

                    if(item.getId() == null)
                        response = context.insertInto(ITEM, ITEM.ITEM_NAME, ITEM.DESCRIPTION, ITEM.BASE_QUANTITY, ITEM.BASE_COST_CP)
                            .values(item.getName(), item.getDescription(), item.getBaseQuantity(), item.getBaseCost())
                            .returning()
                            .fetchOne()
                            .into(Item.class);
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Item, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Item> update (final Item item) {
        return new AbstractServiceResponse<Item>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    if(context.update(ITEM)
                            .set(ITEM.ITEM_NAME, item.getName())
                            .set(ITEM.DESCRIPTION, item.getDescription())
                            .set(ITEM.BASE_QUANTITY, item.getBaseQuantity())
                            .set(ITEM.BASE_COST_CP, item.getBaseCost())
                            .where(ITEM.ITEM_ID.eq(item.getId()))
                            .execute() > 0) {
                        response = item;
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Feature, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(ITEM).where(ITEM.ITEM_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

}
