package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.models.Spell;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER_CHARACTER_SPELL_XREF;
import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.SPELL;

@Service
public class PlayerCharacterSpellService implements NestedService<Spell> {
    private final static Logger LOGGER = LoggerFactory.getLogger(PlayerCharacterSpellService.class);
    private final DSLContext context;

    private final SpellService spellService;

    @Autowired
    public PlayerCharacterSpellService(final DSLContext context, final SpellService spellService) {
        this.context = context;
        this.spellService = spellService;
    }

    public ServiceResponse<List<Spell>> find (final Integer baseId) {
        return new AbstractServiceResponse<List<Spell>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context
                        .select(SPELL.fields())
                        .from(SPELL)
                        .join(PLAYER_CHARACTER_SPELL_XREF).on(PLAYER_CHARACTER_SPELL_XREF.SPELL.eq(SPELL.SPELL_ID))
                        .where(PLAYER_CHARACTER_SPELL_XREF.PLAYER_CHARACTER.eq(baseId))
                        .fetch()
                        .into(Spell.class);
            }
        };
    }

    public ServiceResponse<Spell> find (final Integer baseId, final Integer id) {
        return new AbstractServiceResponse<Spell>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context
                        .select(SPELL.fields())
                        .from(SPELL)
                        .join(PLAYER_CHARACTER_SPELL_XREF).on(PLAYER_CHARACTER_SPELL_XREF.SPELL.eq(SPELL.SPELL_ID))
                        .where(PLAYER_CHARACTER_SPELL_XREF.PLAYER_CHARACTER.eq(baseId))
                        .and(SPELL.SPELL_ID.eq(id))
                        .fetchOne()
                        .into(Spell.class);
            }
        };
    }

    public ServiceResponse<Spell> create(final Integer baseId, final Spell spell) {
        return new AbstractServiceResponse<Spell>() {
            @Override
            public void execute() {
                try {
                    if(spell.getSpellId() == null) {
                        Record1<Integer> idRecord = context.select(SPELL.SPELL_ID).from(SPELL)
                                .where(SPELL.SPELL_NAME.eq(spell.getSpellName()))
                                .fetchOne();

                        if(idRecord != null)
                            spell.setSpellId(idRecord.value1());
                    }

                    if(spell.getSpellId() == null)
                        spell.setSpellId(spellService.create(spell).getResponse().getSpellId());

                    response = find(baseId,
                            context.insertInto(PLAYER_CHARACTER_SPELL_XREF,
                                    PLAYER_CHARACTER_SPELL_XREF.SPELL,
                                    PLAYER_CHARACTER_SPELL_XREF.PLAYER_CHARACTER)
                                    .values(spell.getSpellId(), baseId)
                                    .returning(PLAYER_CHARACTER_SPELL_XREF.SPELL)
                                    .fetchOne()
                                    .getSpell()).getResponse();
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Spell, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }

                status = HttpStatus.OK;
            }
        };
    }

    public ServiceResponse<Spell> update(final Integer baseId, final Spell spell) {
        return spellService.update(spell);
    }
}
