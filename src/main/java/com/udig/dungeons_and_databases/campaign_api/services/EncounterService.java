package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.CreatureEncounterXrefRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Creature;
import com.udig.dungeons_and_databases.campaign_api.models.Encounter;
import com.udig.dungeons_and_databases.campaign_api.models.PlayerCharacter;
import org.jooq.*;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.*;
import static com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.Terrain.TERRAIN;
import static org.jooq.impl.DSL.inline;

@Service
public class EncounterService implements BaseService<Encounter> {
    private final static Logger LOGGER = LoggerFactory.getLogger(EncounterService.class);
    private final DSLContext context;
    private final PlayerCharacterService playerCharacterService;

    @Autowired
    public EncounterService(DSLContext context, PlayerCharacterService playerCharacterService) {
        this.context = context;
        this.playerCharacterService = playerCharacterService;
    }

    public ServiceResponse<List<Encounter>> find() {
        return new AbstractServiceResponse<List<Encounter>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(ENCOUNTER.fields())
                        .from(ENCOUNTER)
                        .fetch()
                        .into(Encounter.class);
            }
        };
    }


    public ServiceResponse<Encounter> find (final Integer id) {
        return new AbstractServiceResponse<Encounter>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(ENCOUNTER.fields())
                        .from(ENCOUNTER)
                        .where(ENCOUNTER.ENCOUNTER_ID.eq(id))
                        .fetchOne()
                        .into(Encounter.class);
            }
        };
    }

    public ServiceResponse<Encounter> create (final Encounter encounter) {
        return new AbstractServiceResponse<Encounter>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    response = find(context.insertInto(ENCOUNTER,
                            ENCOUNTER.CHALLENGE_RATING, ENCOUNTER.EXPERIENCE_REWARD, ENCOUNTER.TERRAIN)
                            .values(
                                    encounter.getChallengeRating(),
                                    encounter.getExperienceReward(),
                                    encounter.getTerrain()
                            )
                            .returning(ENCOUNTER.ENCOUNTER_ID)
                            .fetchOne().getEncounterId()).getResponse();
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Encounter, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Encounter> update (final Encounter encounter) {
        return new AbstractServiceResponse<Encounter>() {
            @Override
            public void execute() {
                try {
                    response = find(
                            context.update(ENCOUNTER)
                            .set(ENCOUNTER.CHALLENGE_RATING, encounter.getChallengeRating())
                            .set(ENCOUNTER.EXPERIENCE_REWARD, encounter.getExperienceReward())
                            .set(ENCOUNTER.TERRAIN, encounter.getTerrain())
                            .returning(ENCOUNTER.ENCOUNTER_ID)
                            .fetchOne()
                            .getEncounterId()).getResponse();

                    status = HttpStatus.OK;
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Encounter, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Encounter> generate(final Encounter encounter, final Integer partyId) {
        return new AbstractServiceResponse<Encounter>() {
            @Override
            public void execute() {
                List<PlayerCharacter> playersInParty = playerCharacterService.findForParty(partyId).getResponse();

                Encounter enc;
                if(encounter.getId() == null || encounter.getId() == 0)
                        enc = create(encounter).getResponse();
                else
                    enc = encounter;


                SelectJoinStep<Record2<Integer, Integer>> query =
                        context.select(CREATURE.CREATURE_ID, inline(enc.getId())).from(CREATURE);

                if(encounter.getTerrain() > 0)
                    query.join(CREATURE_TERRAIN_XREF).on(CREATURE.CREATURE_ID.eq(CREATURE_TERRAIN_XREF.CREATURE))
                            .join(TERRAIN).on(CREATURE_TERRAIN_XREF.TERRAIN.eq(TERRAIN.TERRAIN_ID))
                            .where(TERRAIN.TERRAIN_ID.eq(enc.getTerrain()));

                query.where(CREATURE.CHALLENGE_RATING.eq(Double.valueOf(playersInParty.size())))
                        .limit(Math.max(1, (int) Math.round(Math.random() * playersInParty.size())));


                InsertValuesStep2<CreatureEncounterXrefRecord, Integer, Integer> insertQuery = context
                        .insertInto(CREATURE_ENCOUNTER_XREF, CREATURE_ENCOUNTER_XREF.CREATURE, CREATURE_ENCOUNTER_XREF.ENCOUNTER);

                for(Record2<Integer, Integer> record : query.fetch())
                    insertQuery.values(record.value1(), record.value2());



                insertQuery.execute();

                response = enc;
                status = HttpStatus.OK;
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(ENCOUNTER).where(ENCOUNTER.ENCOUNTER_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

    public ServiceResponse<List<Integer>> creaturesForEncounter(Integer encounterId) {
        return new AbstractServiceResponse<List<Integer>> () {
            @Override
            public void execute() {
                response = context.select(CREATURE_ENCOUNTER_XREF.CREATURE).from(CREATURE_ENCOUNTER_XREF)
                        .where(CREATURE_ENCOUNTER_XREF.ENCOUNTER.eq(encounterId))
                        .fetch(CREATURE_ENCOUNTER_XREF.CREATURE);
                status = HttpStatus.OK;
            }
        };
    }
}
