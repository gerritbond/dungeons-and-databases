package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.FeatureRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Feature;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.FEATURE;

@Service
public class FeatureService implements BaseService<Feature> {
    private final static Logger LOGGER = LoggerFactory.getLogger(FeatureService.class);
    private final DSLContext context;

    @Autowired
    public FeatureService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Feature>> find() {
        return new AbstractServiceResponse<List<Feature>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(FEATURE.fields())
                        .from(FEATURE)
                        .fetch()
                        .into(Feature.class);
            }
        };
    }


    public ServiceResponse<Feature> find (final Integer id) {
        return new AbstractServiceResponse<Feature>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(FEATURE.fields())
                        .from(FEATURE)
                        .where(FEATURE.FEATURE_ID.eq(id))
                        .fetchOne()
                        .into(Feature.class);
            }
        };
    }

    public ServiceResponse<Feature> create (final Feature feature) {
        return new AbstractServiceResponse<Feature>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    response = context.insertInto(FEATURE, FEATURE.FEATURE_NAME, FEATURE.DESCRIPTION, FEATURE.SKILL)
                            .values(feature.getName(), feature.getDescription(), feature.getSkill().getId())
                            .returning()
                            .fetchOne()
                            .into(Feature.class);
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Feature, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Feature> update (final Feature feature) {
        return new AbstractServiceResponse<Feature>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    FeatureRecord featureRecord = context.newRecord(FEATURE, feature);
                    response = feature;

                    if(featureRecord.update(FEATURE.FEATURE_NAME, FEATURE.DESCRIPTION, FEATURE.SKILL) > 0) {
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Feature, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(FEATURE).where(FEATURE.FEATURE_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

}
