package com.udig.dungeons_and_databases.campaign_api.services;

import org.springframework.http.ResponseEntity;

public interface ServiceResponse<T> {
    ResponseEntity<T> toResponseEntity ();
    T getResponse();
    void execute ();
}
