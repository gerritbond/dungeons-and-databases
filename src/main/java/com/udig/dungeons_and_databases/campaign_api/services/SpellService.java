package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.SpellRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Spell;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.SPELL;

@Service
public class SpellService implements BaseService<Spell> {
    private final static Logger LOGGER = LoggerFactory.getLogger(SpellService.class);
    private final DSLContext context;

    @Autowired
    public SpellService (final DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Spell>> find () {
        return new AbstractServiceResponse<List<Spell>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context
                        .select(SPELL.fields())
                        .from(SPELL)
                        .fetch()
                        .into(Spell.class);
            }
        };
    }

    public ServiceResponse<Spell> find (final Integer id) {
        return new AbstractServiceResponse<Spell>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context
                        .select(SPELL.fields())
                        .from(SPELL).where(SPELL.SPELL_ID.eq(id))
                        .fetchOne()
                        .into(Spell.class);
            }
        };
    }

    public ServiceResponse<Spell> create(final Spell spell) {
        return new AbstractServiceResponse<Spell>() {
            @Override
            public void execute() {
                try {
                    SpellRecord spellRecord = context.newRecord(SPELL, spell);
                    context.executeInsert(spellRecord);

                    response = context.select(SPELL.fields())
                            .from(SPELL)
                            .where(SPELL.SPELL_NAME.eq(spell.getSpellName()))
                            .fetchOne()
                            .into(Spell.class);

                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Spell, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }

                status = HttpStatus.OK;
            }
        };
    }

    public ServiceResponse<Spell> update(final Spell updates) {
        return new AbstractServiceResponse<Spell>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                SpellRecord spellRecord = context.newRecord(SPELL, updates);
                Integer updatedRecords = context.executeUpdate(spellRecord, SPELL.SPELL_ID.eq(updates.getSpellId()));

                if(updatedRecords > 0)
                    response = updates;
                else
                    status = HttpStatus.BAD_REQUEST;

            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(SPELL).where(SPELL.SPELL_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }


}
