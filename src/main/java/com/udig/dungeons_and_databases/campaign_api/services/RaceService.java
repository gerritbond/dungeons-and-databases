package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.RaceRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Race;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.RACE;

@Service
public class RaceService implements BaseService<Race> {
    private final static Logger LOGGER = LoggerFactory.getLogger(RaceService.class);
    private final DSLContext context;

    @Autowired
    public RaceService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Race>> find() {
        return new AbstractServiceResponse<List<Race>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(RACE.fields())
                        .from(RACE)
                        .fetch()
                        .into(Race.class);
            }
        };
    }


    public ServiceResponse<Race> find (final Integer id) {
        return new AbstractServiceResponse<Race>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(RACE.fields())
                        .from(RACE)
                        .where(RACE.RACE_ID.eq(id))
                        .fetchOne()
                        .into(Race.class);
            }
        };
    }

    public ServiceResponse<Race> create (final Race race) {
        return new AbstractServiceResponse<Race>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    response = context.insertInto(RACE, RACE.RACE_NAME, RACE.DESCRIPTION)
                            .values(race.getName(), race.getDescription())
                            .returning()
                            .fetchOne()
                            .into(Race.class);
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Race, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Race> update (final Race race) {
        return new AbstractServiceResponse<Race>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    RaceRecord raceRecord = context.newRecord(RACE, race);
                    response = race;

                    if(raceRecord.update(RACE.RACE_NAME, RACE.DESCRIPTION) > 0) {
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Race, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(RACE).where(RACE.RACE_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

}
