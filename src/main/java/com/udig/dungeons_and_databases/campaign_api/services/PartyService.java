package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.models.Party;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PARTY;

@Service
public class PartyService implements BaseService <Party> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PartyService.class);
    private final DSLContext context;

    private PlayerCharacterService playerCharacterService;

    @Autowired
    public PartyService(final DSLContext context, final PlayerCharacterService playerCharacterService) {
        this.context = context;
        this.playerCharacterService = playerCharacterService;
    }

    public ServiceResponse<List<Party>> find () {
        return new AbstractServiceResponse<List<Party>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context
                        .select(PARTY.fields()).select(PARTY.player().fields())
                        .from(PARTY)
                        .fetch()
                        .into(Party.class);

                for(Party party : response) {
                    party.setCharacters(playerCharacterService.findForParty(party.getId()).getResponse());
                }
            }
        };
    }

    public ServiceResponse<Party> find (final Integer id) {
        return new AbstractServiceResponse<Party>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context
                    .select(PARTY.fields()).select(PARTY.player().fields())
                    .from(PARTY).where(PARTY.PARTY_ID.eq(id))
                    .fetchOne()
                    .into(Party.class);
            }
        };
    }

    public ServiceResponse<List<Party>> findAll(List<Integer> partyIds) {
        return new AbstractServiceResponse<List<Party>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context
                        .select(PARTY.fields()).select(PARTY.player().fields())
                        .from(PARTY)
                        .where(PARTY.PARTY_ID.in(partyIds))
                        .fetch()
                        .into(Party.class);

                for(Party party : response) {
                    party.setCharacters(playerCharacterService.findForParty(party.getId()).getResponse());
                }
            }
        };
    }

    public ServiceResponse<Party> create(final Party party) {
        return new AbstractServiceResponse<Party>() {
            @Override
            public void execute() {
                try {
                    final Integer partyId = context.insertInto(PARTY, PARTY.GAME_MASTER, PARTY.NAME)
                        .values(party.getGameMaster().getId(), party.getName())
                        .returning(PARTY.PARTY_ID)
                        .fetchOne()
                        .getPartyId();

                    response = context.select(PARTY.PARTY_ID, PARTY.NAME, PARTY.player().PLAYER_ID, PARTY.player().FIRST_NAME, PARTY.player().LAST_NAME)
                            .from(PARTY)
                            .where(PARTY.PARTY_ID.eq(partyId))
                            .fetchOne()
                            .into(Party.class);

                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Party, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }

                status = HttpStatus.OK;
            }
        };
    }

    public ServiceResponse<Party> update(final Party updates) {
        return new AbstractServiceResponse<Party>() {
            @Override
            public void execute() {
                context.update(PARTY)
                    .set(PARTY.NAME, updates.getName())
                    .set(PARTY.GAME_MASTER, updates.getGameMaster().getId())
                    .where(PARTY.PARTY_ID.eq(updates.getId()));

                ResponseEntity<Party> updatedParty = find(updates.getId()).toResponseEntity();
                this.response = updatedParty.getBody();
                this.status = updatedParty.getStatusCode();

                if(this.response.getName().equals(updates.getName())
                        && this.response.getGameMaster().getId().equals(updates.getGameMaster().getId()))
                    this.status = HttpStatus.BAD_REQUEST;
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(PARTY).where(PARTY.PARTY_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

}
