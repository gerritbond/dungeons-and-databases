package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.PlayerCharacterRecord;
import com.udig.dungeons_and_databases.campaign_api.models.PlayerCharacter;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER_CHARACTER;

@Service
public class PlayerCharacterService implements BaseService<PlayerCharacter> {
    private final static Logger LOGGER = LoggerFactory.getLogger(PlayerCharacterService.class);
    private final DSLContext context;

    @Autowired
    public PlayerCharacterService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<PlayerCharacter>> find() {
        return new AbstractServiceResponse<List<PlayerCharacter>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(PLAYER_CHARACTER.fields())
                        .from(PLAYER_CHARACTER)
                        .fetch()
                        .into(PlayerCharacter.class);
            }
        };
    }


    public ServiceResponse<PlayerCharacter> find (final Integer id) {
        return new AbstractServiceResponse<PlayerCharacter>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(PLAYER_CHARACTER.fields())
                        .from(PLAYER_CHARACTER)
                        .where(PLAYER_CHARACTER.CHARACTER_ID.eq(id))
                        .fetchOne()
                        .into(PlayerCharacter.class);
            }
        };
    }

    public ServiceResponse<List<PlayerCharacter>> findForParty(Integer partyId) {
        return new AbstractServiceResponse<List<PlayerCharacter>>() {
            @Override
            public void execute() {

                status = HttpStatus.OK;
                response = context.select(PLAYER_CHARACTER.fields())
                        .from(PLAYER_CHARACTER)
                        .where(PLAYER_CHARACTER.PARTY.eq(partyId))
                        .fetch()
                        .into(PlayerCharacter.class);
            }
        };
    }

    public ServiceResponse<List<PlayerCharacter>> findForPlayer(Integer playerId) {
        return new AbstractServiceResponse<List<PlayerCharacter>>() {
            @Override
            public void execute() {

                status = HttpStatus.OK;
                response = context.select(PLAYER_CHARACTER.fields())
                        .from(PLAYER_CHARACTER)
                        .where(PLAYER_CHARACTER.PLAYER.eq(playerId))
                        .fetch()
                        .into(PlayerCharacter.class);
            }
        };
    }


    public ServiceResponse<PlayerCharacter> create (final PlayerCharacter character) {
        return new AbstractServiceResponse<PlayerCharacter>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                     response = find(context.insertInto(PLAYER_CHARACTER,
                            PLAYER_CHARACTER.CHARACTER_NAME, PLAYER_CHARACTER.BACKGROUND,
                            PLAYER_CHARACTER.BACKSTORY, PLAYER_CHARACTER.ALIGNMENT,
                            PLAYER_CHARACTER.EXPERIENCE, PLAYER_CHARACTER.ARMOR_CLASS,
                            PLAYER_CHARACTER.INSPIRATION, PLAYER_CHARACTER.PROFICIENCY_BONUS,
                            PLAYER_CHARACTER.AGE, PLAYER_CHARACTER.HEIGHT,
                            PLAYER_CHARACTER.WEIGHT, PLAYER_CHARACTER.EYES,
                            PLAYER_CHARACTER.SKIN, PLAYER_CHARACTER.HAIR,
                            PLAYER_CHARACTER.HITPOINTS, PLAYER_CHARACTER.CURRENT_HITPOINTS,
                            PLAYER_CHARACTER.TEMPORARY_HITPOINTS, PLAYER_CHARACTER.PERSONALITY_TRAITS,
                            PLAYER_CHARACTER.IDEALS, PLAYER_CHARACTER.BONDS,
                            PLAYER_CHARACTER.FLAWS,
                            PLAYER_CHARACTER.RACE, PLAYER_CHARACTER.PLAYER, PLAYER_CHARACTER.PARTY)
                            .values(
                                character.getName(), character.getBackground(),
                                character.getBackstory(), character.getAlignment().ordinal(),
                                character.getExperience(), character.getArmorClass(),
                                character.getInspiration(), character.getProficiencyBonus(),
                                character.getAge(), character.getHeight(),
                                character.getWeight(), character.getEyes(),
                                character.getSkin(), character.getHair(),
                                character.getHitpoints(), character.getCurrentHitpoints(),
                                character.getTemporaryHitpoints(), character.getPersonalityTraits(),
                                character.getIdeals(), character.getBonds(),
                                character.getFlaws(), character.getRace(),
                                character.getPlayer(), character.getParty())
                            .returning(PLAYER_CHARACTER.CHARACTER_ID)
                            .fetchOne()
                            .getCharacterId())
                            .getResponse();
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model PlayerCharacter, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<PlayerCharacter> update (final PlayerCharacter playerCharacter) {
        return new AbstractServiceResponse<PlayerCharacter>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    PlayerCharacterRecord playerCharacterRecord = context.newRecord(PLAYER_CHARACTER, playerCharacter);
                    response = playerCharacter;

                    if(playerCharacterRecord.update(PLAYER_CHARACTER.CHARACTER_NAME, PLAYER_CHARACTER.BACKGROUND,
                            PLAYER_CHARACTER.BACKSTORY, PLAYER_CHARACTER.ALIGNMENT,
                            PLAYER_CHARACTER.EXPERIENCE,
                            PLAYER_CHARACTER.INSPIRATION, PLAYER_CHARACTER.PROFICIENCY_BONUS,
                            PLAYER_CHARACTER.AGE, PLAYER_CHARACTER.HEIGHT,
                            PLAYER_CHARACTER.WEIGHT, PLAYER_CHARACTER.EYES,
                            PLAYER_CHARACTER.SKIN, PLAYER_CHARACTER.HAIR,
                            PLAYER_CHARACTER.HITPOINTS, PLAYER_CHARACTER.CURRENT_HITPOINTS,
                            PLAYER_CHARACTER.TEMPORARY_HITPOINTS, PLAYER_CHARACTER.PERSONALITY_TRAITS,
                            PLAYER_CHARACTER.IDEALS, PLAYER_CHARACTER.BONDS,
                            PLAYER_CHARACTER.FLAWS,
                            PLAYER_CHARACTER.RACE, PLAYER_CHARACTER.PLAYER) > 0) {
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model PlayerCharacter, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(PLAYER_CHARACTER).where(PLAYER_CHARACTER.CHARACTER_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

}
