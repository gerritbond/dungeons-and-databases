package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.SkillRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Skill;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.RACE;
import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.SKILL;

@Service
public class SkillService implements BaseService<Skill> {
    private final static Logger LOGGER = LoggerFactory.getLogger(SkillService.class);
    private final DSLContext context;

    @Autowired
    public SkillService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Skill>> find() {
        return new AbstractServiceResponse<List<Skill>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(SKILL.fields())
                        .from(SKILL)
                        .fetch()
                        .into(Skill.class);
            }
        };
    }


    public ServiceResponse<Skill> find (final Integer id) {
        return new AbstractServiceResponse<Skill>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(RACE.fields())
                        .from(SKILL)
                        .where(SKILL.SKILL_ID.eq(id))
                        .fetchOne()
                        .into(Skill.class);
            }
        };
    }

    public ServiceResponse<Skill> create (final Skill skill) {
        return new AbstractServiceResponse<Skill>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    response = context.insertInto(SKILL, SKILL.SKILL_NAME, SKILL.ABILITY)
                            .values(skill.getName(), skill.getAbility().getId())
                            .returning()
                            .fetchOne()
                            .into(Skill.class);
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Skill, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Skill> update (final Skill skill) {
        return new AbstractServiceResponse<Skill>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    SkillRecord skillRecord = context.newRecord(SKILL, skill);
                    response = skill;

                    if(skillRecord.update(SKILL.SKILL_NAME, SKILL.ABILITY) > 0) {
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Skill, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(SKILL).where(SKILL.SKILL_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

}
