package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.models.Creature;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.CREATURE;

@Service
public class CreatureService implements BaseService<Creature> {
    private final static Logger LOGGER = LoggerFactory.getLogger(CreatureService.class);
    private final DSLContext context;

    @Autowired
    public CreatureService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Creature>> find() {
        return new AbstractServiceResponse<List<Creature>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(CREATURE.fields())
                        .from(CREATURE)
                        .fetch()
                        .into(Creature.class);
            }
        };
    }


    public ServiceResponse<Creature> find (final Integer id) {
        return new AbstractServiceResponse<Creature>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(CREATURE.fields())
                        .from(CREATURE)
                        .where(CREATURE.CREATURE_ID.eq(id))
                        .fetchOne()
                        .into(Creature.class);
            }
        };
    }

    public ServiceResponse<List<Creature>> find (final Integer ... ids) {
        return new AbstractServiceResponse<List<Creature>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(CREATURE.fields())
                        .from(CREATURE)
                        .where(CREATURE.CREATURE_ID.in(ids))
                        .fetch()
                        .into(Creature.class);
            }
        };
    }

    public ServiceResponse<Creature> create (final Creature creature) {
        return new AbstractServiceResponse<Creature>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    response = find(context.insertInto(CREATURE,
                            CREATURE.CREATURE_NAME, CREATURE.ALIGNMENT, CREATURE.SIZE, CREATURE.TYPE,
                            CREATURE.ARMOR_CLASS, CREATURE.HITPOINTS, CREATURE.HITDICE, CREATURE.SPEED,
                            CREATURE.CHALLENGE_RATING, CREATURE.EXPERIENCE_REWARD)
                            .values(
                                    creature.getCreatureName(),
                                    creature.getAlignment().ordinal(),
                                    creature.getSize().ordinal(),
                                    creature.getType().ordinal(),
                                    creature.getArmorClass(),
                                    creature.getHitPoints(),
                                    creature.getHitDice(),
                                    creature.getSpeed(),
                                    creature.getChallengeRating(),
                                    creature.getExperienceReward()
                            )
                            .returning(CREATURE.CREATURE_ID)
                            .fetchOne().getCreatureId()).getResponse();
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Creature, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Creature> update (final Creature creature) {
        return new AbstractServiceResponse<Creature>() {
            @Override
            public void execute() {
                try {
                    response = find(
                            context.update(CREATURE)
                            .set(CREATURE.CREATURE_NAME, creature.getCreatureName())
                            .set(CREATURE.ALIGNMENT, creature.getAlignment().ordinal())
                            .set(CREATURE.SIZE, creature.getSize().ordinal())
                            .set(CREATURE.TYPE, creature.getType().ordinal())
                            .set(CREATURE.ARMOR_CLASS, creature.getArmorClass())
                            .set(CREATURE.HITPOINTS, creature.getHitPoints())
                            .set(CREATURE.HITDICE, creature.getHitDice())
                            .set(CREATURE.SPEED, creature.getSpeed())
                            .set(CREATURE.CHALLENGE_RATING, creature.getChallengeRating())
                            .set(CREATURE.EXPERIENCE_REWARD, creature.getExperienceReward())
                            .returning(CREATURE.CREATURE_ID)
                            .fetchOne()
                            .getCreatureId()).getResponse();

                    status = HttpStatus.OK;
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Creature, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }


    @Override
    public ServiceResponse<Boolean> delete(Integer id) {
        return new AbstractServiceResponse<Boolean>() {
            @Override
            public void execute() {
                response = context.delete(CREATURE).where(CREATURE.CREATURE_ID.eq(id)).execute() > 0;
                status = HttpStatus.OK;
            }
        };
    }

}
