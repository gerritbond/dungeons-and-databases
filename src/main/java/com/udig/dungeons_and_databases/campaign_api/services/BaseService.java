package com.udig.dungeons_and_databases.campaign_api.services;

import java.util.List;

public interface BaseService <T> {
    ServiceResponse<List<T>> find ();
    ServiceResponse<T> find (final Integer id);
    ServiceResponse<T> create (final T t);
    ServiceResponse<T> update (final T t);
    ServiceResponse<Boolean> delete (final Integer id);
}
