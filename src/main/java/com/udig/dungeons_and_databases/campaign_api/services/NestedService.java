package com.udig.dungeons_and_databases.campaign_api.services;

import java.util.List;

public interface NestedService <T> {
    ServiceResponse<List<T>> find (final Integer baseId);
    ServiceResponse<T> find (final Integer baseId, final Integer id);
    ServiceResponse<T> create (final Integer baseId, final T t);
    ServiceResponse<T> update (final Integer baseId, final T t);
}
