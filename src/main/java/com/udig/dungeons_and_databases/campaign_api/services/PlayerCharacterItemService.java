package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.models.Item;
import com.udig.dungeons_and_databases.campaign_api.utils.Util;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.ITEM;
import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER_CHARACTER_ITEM_XREF;

@Service
public class PlayerCharacterItemService implements NestedService<Item> {
    private final static Logger LOGGER = LoggerFactory.getLogger(PlayerCharacterItemService.class);
    private final ItemService itemService;
    private final DSLContext context;

    @Autowired
    public PlayerCharacterItemService(DSLContext context, ItemService itemService) {
        this.context = context;
        this.itemService = itemService;
    }

    public ServiceResponse<List<Item>> find(final Integer baseId) {
        return new AbstractServiceResponse<List<Item>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(Util.mergeFields(ITEM.fields(), PLAYER_CHARACTER_ITEM_XREF.fields()))
                        .from(ITEM)
                        .join(PLAYER_CHARACTER_ITEM_XREF).on(PLAYER_CHARACTER_ITEM_XREF.ITEM.eq(ITEM.ITEM_ID))
                        .where(PLAYER_CHARACTER_ITEM_XREF.PLAYER_CHARACTER.eq(baseId))
                        .fetch()
                        .into(Item.class);
            }
        };
    }


    public ServiceResponse<Item> find (final Integer baseId, final Integer id) {
        return new AbstractServiceResponse<Item>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(Util.mergeFields(ITEM.fields(), PLAYER_CHARACTER_ITEM_XREF.fields()))
                        .from(ITEM)
                        .join(PLAYER_CHARACTER_ITEM_XREF).on(PLAYER_CHARACTER_ITEM_XREF.ITEM.eq(ITEM.ITEM_ID))
                        .where(PLAYER_CHARACTER_ITEM_XREF.PLAYER_CHARACTER.eq(baseId))
                        .and(ITEM.ITEM_ID.eq(id))
                        .fetchOne()
                        .into(Item.class);
            }
        };
    }

    public ServiceResponse<Item> create(final Integer baseId, final Item item) {
        return new AbstractServiceResponse<Item>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    if(item.getId() == null) {
                        Record1<Integer> idRecord = context.select(ITEM.ITEM_ID).from(ITEM)
                                .where(ITEM.ITEM_NAME.eq(item.getName()))
                                .fetchOne();

                        if(idRecord != null)
                            item.setId(idRecord.value1());
                    }

                    if(item.getId() == null)
                        item.setId(
                                context.insertInto(ITEM, ITEM.ITEM_NAME, ITEM.DESCRIPTION)
                                        .values(item.getName(), item.getDescription())
                                        .returning(ITEM.ITEM_ID)
                                        .fetchOne()
                                        .getItemId());

                    response = find(baseId,
                            context.insertInto(PLAYER_CHARACTER_ITEM_XREF,
                                    PLAYER_CHARACTER_ITEM_XREF.ITEM,
                                    PLAYER_CHARACTER_ITEM_XREF.PLAYER_CHARACTER,
                                    PLAYER_CHARACTER_ITEM_XREF.CURRENT_COST_CP,
                                    PLAYER_CHARACTER_ITEM_XREF.CURRENT_QUANTITY)
                                    .values(item.getId(), baseId, item.getCurrentCost(), item.getCurrentQuantity())
                                    .returning(PLAYER_CHARACTER_ITEM_XREF.ITEM)
                                    .fetchOne()
                                    .getItem()).getResponse();
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Item, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Item> update (final Integer baseId, final Item item) {
        return new AbstractServiceResponse<Item>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    if(context.update(PLAYER_CHARACTER_ITEM_XREF)
                            .set(PLAYER_CHARACTER_ITEM_XREF.CURRENT_COST_CP, item.getCurrentCost())
                            .set(PLAYER_CHARACTER_ITEM_XREF.CURRENT_QUANTITY, item.getCurrentQuantity())
                            .where(PLAYER_CHARACTER_ITEM_XREF.PLAYER_CHARACTER.eq(baseId).and(PLAYER_CHARACTER_ITEM_XREF.ITEM.eq(item.getId())))
                            .execute() > 0) {
                        response = item;
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Feature, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }
}
