package com.udig.dungeons_and_databases.campaign_api.services;

import com.udig.dungeons_and_databases.campaign_api.generated.public_.tables.records.OrganizationRecord;
import com.udig.dungeons_and_databases.campaign_api.models.Organization;
import com.udig.dungeons_and_databases.campaign_api.utils.Util;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.ORGANIZATION;
import static com.udig.dungeons_and_databases.campaign_api.generated.public_.Tables.PLAYER_CHARACTER_ORGANIZATION_XREF;

@Service
public class OrganizationService implements NestedService<Organization> {
    private final static Logger LOGGER = LoggerFactory.getLogger(OrganizationService.class);
    private final DSLContext context;

    @Autowired
    public OrganizationService(DSLContext context) {
        this.context = context;
    }

    public ServiceResponse<List<Organization>> find(final Integer baseId) {
        return new AbstractServiceResponse<List<Organization>>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(Util.mergeFields(ORGANIZATION.fields(), PLAYER_CHARACTER_ORGANIZATION_XREF.fields()))
                        .from(ORGANIZATION)
                        .join(PLAYER_CHARACTER_ORGANIZATION_XREF).on(PLAYER_CHARACTER_ORGANIZATION_XREF.ORGANIZATION.eq(ORGANIZATION.ORGANIZATION_ID))
                        .where(PLAYER_CHARACTER_ORGANIZATION_XREF.PLAYER_CHARACTER.eq(baseId))
                        .fetch()
                        .into(Organization.class);
            }
        };
    }


    public ServiceResponse<Organization> find (final Integer baseId, final Integer id) {
        return new AbstractServiceResponse<Organization>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;
                response = context.select(Util.mergeFields(ORGANIZATION.fields(), PLAYER_CHARACTER_ORGANIZATION_XREF.fields()))
                        .from(ORGANIZATION)
                        .join(PLAYER_CHARACTER_ORGANIZATION_XREF).on(PLAYER_CHARACTER_ORGANIZATION_XREF.ORGANIZATION.eq(ORGANIZATION.ORGANIZATION_ID))
                        .where(PLAYER_CHARACTER_ORGANIZATION_XREF.PLAYER_CHARACTER.eq(baseId))
                        .and(ORGANIZATION.ORGANIZATION_ID.eq(id))
                        .fetchOne()
                        .into(Organization.class);
            }
        };
    }

    public ServiceResponse<Organization> create (final Integer baseId, final Organization organization) {
        return new AbstractServiceResponse<Organization>() {
            @Override
            public void execute() {
                status = HttpStatus.OK;

                try {
                    if(organization.getId() == null) {
                        Record1<Integer> idRecord = context.select(ORGANIZATION.ORGANIZATION_ID).from(ORGANIZATION)
                                .where(ORGANIZATION.ORGANIZATION_NAME.eq(organization.getName()))
                                .fetchOne();

                        if(idRecord != null)
                            organization.setId(idRecord.value1());
                    }

                    if(organization.getId() == null)
                        organization.setId(
                                context.insertInto(ORGANIZATION, ORGANIZATION.ORGANIZATION_NAME, ORGANIZATION.DESCRIPTION)
                                        .values(organization.getName(), organization.getDescription())
                                        .returning(ORGANIZATION.ORGANIZATION_ID)
                                        .fetchOne()
                                        .getOrganizationId());

                    response = find(baseId,
                            context.insertInto(PLAYER_CHARACTER_ORGANIZATION_XREF,
                                    PLAYER_CHARACTER_ORGANIZATION_XREF.ORGANIZATION,
                                    PLAYER_CHARACTER_ORGANIZATION_XREF.PLAYER_CHARACTER)
                                    .values(organization.getId(), baseId)
                                    .returning(PLAYER_CHARACTER_ORGANIZATION_XREF.ORGANIZATION)
                                    .fetchOne()
                                    .getOrganization()).getResponse();
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to create model Organization, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }

    public ServiceResponse<Organization> update (final Integer baseId, final Organization organization) {
        return new AbstractServiceResponse<Organization>() {
            @Override
            public void execute() {
                try {
                    response = null;
                    status = HttpStatus.BAD_REQUEST;

                    OrganizationRecord organizationRecord = context.newRecord(ORGANIZATION, organization);
                    response = organization;

                    if(organizationRecord.update(ORGANIZATION.ORGANIZATION_NAME, ORGANIZATION.DESCRIPTION) > 0) {
                        status = HttpStatus.OK;
                    }
                } catch (DataAccessException ex) {
                    LOGGER.warn(String.format("Unable to update model Organization, exception is %s", ex));
                    status = HttpStatus.BAD_REQUEST;
                    response = null;
                }
            }
        };
    }
}
