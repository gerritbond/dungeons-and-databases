CREATE TABLE player_character_spell_xref (
  player_character INTEGER REFERENCES player_character (character_id),
  spell INTEGER REFERENCES spell (spell_id)
);
