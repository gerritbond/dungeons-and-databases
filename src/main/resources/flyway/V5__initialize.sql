ALTER TABLE player_character_feature_xref ADD CONSTRAINT pc_f_xref_unique UNIQUE (feature, player_character);
ALTER TABLE player_character_item_xref ADD CONSTRAINT pc_i_xref_unique UNIQUE (item, player_character);
ALTER TABLE race_ability_xref ADD CONSTRAINT r_a_xref_unique UNIQUE (race, ability);
ALTER TABLE race_skill_xref ADD CONSTRAINT r_s_xref_unique UNIQUE (race, skill);
ALTER TABLE player_character_organization_xref ADD CONSTRAINT pc_o_xref_unique UNIQUE (organization, player_character);
ALTER TABLE player_character_clazz_xref ADD CONSTRAINT pc_c_xref_unique UNIQUE (player_character, clazz);
ALTER TABLE player_character_skill_xref ADD CONSTRAINT pc_s_xref_unique UNIQUE (player_character, skill);
ALTER TABLE player_character_ability_xref ADD CONSTRAINT pc_a_xref_unique UNIQUE (player_character, ability);

ALTER TABLE item DROP COLUMN item_type;
DROP TABLE item_type;

ALTER TABLE player_character_item_xref ADD current_quantity INTEGER NOT NULL DEFAULT (1);
ALTER TABLE player_character_item_xref ADD current_cost_cp INTEGER NOT NULL DEFAULT (1);