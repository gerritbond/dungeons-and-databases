CREATE TABLE race (
  race_id SERIAL,
  race_name TEXT NOT NULL,
  description TEXT NOT NULL,
  PRIMARY KEY (race_id)
);

CREATE TABLE player_character (
  character_id SERIAL,
  character_name TEXT NOT NULL,
  background TEXT NOT NULL,
  backstory TEXT NOT NULL,
  alignment INTEGER NOT NULL,
  experience INTEGER NOT NULL DEFAULT (0),
  armor_class INTEGER NOT NULL DEFAULT (10),
  inspiration BOOLEAN NOT NULL DEFAULT (false),
  proficiency_bonus INTEGER NOT NULL DEFAULT (2),
  age INTEGER,
  height INTEGER,
  weight INTEGER,
  eyes TEXT,
  skin TEXT,
  hair TEXT,
  hitpoints INTEGER NOT NULL DEFAULT (0),
  current_hitpoints INTEGER NOT NULL DEFAULT (0),
  temporary_hitpoints INTEGER NOT NULL DEFAULT (0),
  personality_traits TEXT,
  ideals TEXT,
  BONDS TEXT,
  FLAWS TEXT,
  race INTEGER REFERENCES race (race_id),
  player INTEGER REFERENCES player (player_id),
  party INTEGER REFERENCES party (party_id),
  PRIMARY KEY (character_id)
);

CREATE TABLE organization (
  organization_id SERIAL,
  organization_name TEXT NOT NULL,
  description TEXT NOT NULL,
  PRIMARY KEY (organization_id)
);

﻿CREATE TABLE ability (
  ability_id SERIAL,
  ability_name TEXT NOT NULL,
  abbreviation TEXT NOT NULL,
  PRIMARY KEY (ability_id),
  UNIQUE (ability_name)
);


CREATE TABLE skill (
  skill_id SERIAL,
  skill_name TEXT NOT NULL,
  ability INTEGER REFERENCES ability (ability_id),
  PRIMARY KEY (skill_id),
  UNIQUE (skill_name)
);

CREATE TABLE clazz (
  clazz_id SERIAL,
  clazz_name TEXT NOT NULL,
  description TEXT NOT NULL,
  PRIMARY KEY (clazz_id),
  UNIQUE (clazz_name)
);

CREATE TABLE feature (
  feature_id SERIAL,
  feature_name TEXT NOT NULL,
  description TEXT NOT NULL,
  skill INTEGER REFERENCES skill (skill_id),
  PRIMARY KEY (feature_id)
);

CREATE TABLE item_type (
  item_type_id SERIAL,
  item_type_name TEXT NOT NULL,
  description TEXT NOT NULL,
  PRIMARY KEY (item_type_id)
);

CREATE TABLE item (
  item_id SERIAL,
  item_name TEXT NOT NULL,
  description TEXT NOT NULL,
  base_cost_cp INTEGER NOT NULL DEFAULT (1),
  base_quantity INTEGER NOT NULL DEFAULT (1),
  item_type INTEGER REFERENCES item_type (item_type_id),
  PRIMARY KEY (item_id)
);

CREATE TABLE player_character_feature_xref (
  player_character INTEGER REFERENCES player_character (character_id),
  feature INTEGER REFERENCES feature (feature_id)
);

CREATE TABLE player_character_item_xref (
  item INTEGER REFERENCES item (item_id),
  player_character INTEGER REFERENCES player_character (character_id)
);

﻿CREATE TABLE race_ability_xref (
  modifier INTEGER NOT NULL,
  ability INTEGER REFERENCES ability (ability_id),
  race INTEGER REFERENCES race (race_id)
);

CREATE TABLE race_skill_xref (
  race INTEGER REFERENCES race (race_id),
  skill INTEGER REFERENCES skill (skill_id)
);

CREATE TABLE player_character_organization_xref (
  player_character INTEGER REFERENCES player_character (character_id),
  organization INTEGER REFERENCES organization (organization_id)
);

CREATE TABLE player_character_clazz_xref (
  rank INTEGER NOT NULL,
  player_character INTEGER REFERENCES player_character (character_id),
  clazz INTEGER REFERENCES clazz (clazz_id)
);

CREATE TABLE player_character_skill_xref (
  rank INTEGER NOT NULL,
  proficient BOOLEAN NOT NULL,
  player_character INTEGER REFERENCES player_character (character_id),
  skill INTEGER REFERENCES skill (skill_id)
);

﻿CREATE TABLE player_character_ability_xref (
  rank INTEGER NOT NULL,
  proficient BOOLEAN NOT NULL DEFAULT (false),
  player_character INTEGER REFERENCES player_character(character_id),
  ability INTEGER REFERENCES ability (ability_id)
);