CREATE TABLE player_party_xref (
  party INTEGER REFERENCES party (party_id),
  character INTEGER REFERENCES player_character (character_id)
);