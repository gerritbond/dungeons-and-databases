CREATE TABLE party (
  party_id SERIAL,
  name TEXT NULL,
  game_master INTEGER REFERENCES player (player_id),
  PRIMARY KEY(party_id)
);