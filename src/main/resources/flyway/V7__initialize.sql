CREATE TABLE creature (
  creature_id SERIAL,
  creature_name TEXT NOT NULL,
  alignment INTEGER NOT NULL,
  size INTEGER NOT NULL,
  type INTEGER NOT NULL,
  armor_class INTEGER NOT NULL,
  hitpoints INTEGER NOT NULL,
  hitdice INTEGER NOT NULL,
  speed INTEGER NOT NULL,
  challenge_rating FLOAT NOT NULL,
  experience_reward INTEGER NOT NULL,
  PRIMARY KEY (creature_id)
);

CREATE TABLE terrain (
  terrain_id SERIAL,
  terrain_name TEXT NOT NULL,
  description TEXT NOT NULL,
  PRIMARY KEY (terrain_id)
);

CREATE TABLE encounter (
  encounter_id SERIAL,
  challenge_rating FLOAT NOT NULL,
  experience_reward INTEGER NOT NULL,
  terrain INTEGER REFERENCES terrain (terrain_id),
  PRIMARY KEY (encounter_id)
);

CREATE TABLE creature_ability_xref (
  rank INTEGER NOT NULL,
  ability INTEGER REFERENCES ability (ability_id),
  creature INTEGER REFERENCES creature (creature_id),
  PRIMARY KEY (ability, creature)
);

CREATE TABLE creature_spell_xref (
  spell_level INTEGER NOT NULL,
  spell INTEGER REFERENCES spell (spell_id),
  creature INTEGER REFERENCES creature (creature_id),
  PRIMARY  KEY (spell, creature)
);

CREATE TABLE creature_terrain_xref (
  terrain INTEGER REFERENCES terrain (terrain_id),
  creature INTEGER REFERENCES creature (creature_id),
  PRIMARY KEY (terrain, creature)
);

CREATE TABLE creature_encounter_xref (
  creature INTEGER REFERENCES creature (creature_id),
  encounter INTEGER REFERENCES encounter (encounter_id),
  PRIMARY KEY (creature, encounter)
)