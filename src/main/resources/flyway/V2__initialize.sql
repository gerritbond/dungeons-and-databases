CREATE TABLE spell (
  spell_id SERIAL,
  spell_name TEXT NOT NULL,
  casting_time TEXT NOT NULL,
  components TEXT NOT NULL,
  description TEXT NOT NULL,
  duration TEXT NOT NULL,
  level INTEGER NOT NULL,
  range TEXT NOT NULL,
  school TEXT NOT NULL,
  PRIMARY KEY(spell_id),
  UNIQUE(spell_name)
);