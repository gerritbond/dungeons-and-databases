# Dungeons and Databases
## A Fantastical Introduction to Jooq

This repository serves as the application behind a presentation to the Richmond Java Users Group. The presenation details some of the great capabilities of the Jooq library.


Thanks to the following datasources for making loading this application with data significantly more simple:

- [5e-spells](https://github.com/tadzik/5e-spells.git)
- [5e-monsters](https://dl.dropboxusercontent.com/s/iwz112i0bxp2n4a/5e-SRD-Monsters.json)


### Contents
1. Getting Started
2. Project Layout
3. Database
4. Postman
5. Contributors

### Getting Started
Take the following steps and you should have a locally running version of this application:
    
    1. The following dependencies are required:
        - Maven
        - Java 10
        - Docker
    2. Run the create_docker_postgres.sh script found under the 'scripts' folder
    3. At the project root, run mvn spring-boot:run
      

### Project Layout
The core of the Dungeons and Databases project is laid out in a standard Model/View/Controller implemenation.

The 'controllers' packages contains all of the routes used in the API. Those controllers in turn rely on the Service
layer the 'services' package contains those services. In the services the methods use a ServiceResponse wrapper around
the actual work being done to streamline the translation from service response to ResponseEntity when passing it all
the way back up.

In addition to the controllers/services/models packages there is a package for generated sources, 'generated',
a package for Jooq RecordMappers, 'mappers', and the configuration package 'config.'.


### Database
This application is built to run alongside a postgresql database.

The configuration of the required tables in that database are handled by flyway. Once connected to a database, flyway will run the Versioned SQL scripts
sequentially building the database. 

Once flyway has finished running, jooq codegen will execute generating sources for the application to use. These sources are generated to the 
        
        com.udig.dungeons_and_databases.campaign_api.generated
        
package


### Postman
There is a postman collection exported under the postman folder at project root.

This collection provides a ready-built mechanism to hit the API routes provided in this application.

### Contributors
- Gerrit Bond [gerrit.bond@udig.com](gerrit.bond@udig.com) [gerritbond@gmail.com](gerritbond@gmail.com), 
- Andrew Ross [andrew.ross@udig.com](andrew.ross@udig.com) 
